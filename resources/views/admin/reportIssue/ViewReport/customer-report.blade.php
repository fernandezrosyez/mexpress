@extends('admin.layout.base')

@section('title', 'Customer\'s Report Issues')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Customer's Report Issues
            </h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Customer Name</th>
                        <th>Subject</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ReportIssue as $index => $reportIssue)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        @if(isset($reportIssue->customer))
                        <td>{{ $reportIssue->customer->first_name }} {{ $reportIssue->customer->last_name }}</td>
                        @else
                        <td>-</td>
                        @endif
                        <td>{{ $reportIssue->reportSubject->subject }}</td>
                        <td>{{date('Y-m-d h:i A',strtotime($reportIssue->created_at))}}</td>
                        <td>
                            <a href="{{ route('admin.reportIssue.details', [$reportIssue->issues_id,'customer']) }}" class="btn btn-info">View</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Customer Name</th>
                        <th>Subject</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection