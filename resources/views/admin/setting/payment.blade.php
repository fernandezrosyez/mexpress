@extends('admin.layout.base')
@section('title', 'Payment Setting')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Payment Setting</h5>

            <div class="form-group row">
                <div class="col-xs-4"></div>

                <div class="col-xs-6">
                    <div class="onoffswitch">
                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox stripe_check" id="myonoffswitch" tabindex="0" onchange="cardselect()">
                        <label class="onoffswitch-label" for="myonoffswitch">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>

            <form id="stripe_field">
               
                    <input type="hidden" name="type" id="type">

                    <div class="form-group row">
                        <label for="stripe_secret_key" class="col-xs-4 col-form-label">Stripe Secret key</label>
                        <div class="col-xs-8">
                            <input class="form-control" type="text" value="" name="stripe_secret_key" id="stripe_secret_key"  placeholder="Stripe Secret key">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="stripe_publishable_key" class="col-xs-4 col-form-label">Stripe Publishable key</label>
                        <div class="col-xs-8">
                            <input class="form-control" type="text" value="" name="stripe_publishable_key" id="stripe_publishable_key"  placeholder="Stripe Publishable key">
                        </div>
                    </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-4 col-form-label"></label>
                    <div class="col-xs-8">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
           
        </div>
    </div>
</div>
<style>
    .onoffswitch {
        position: relative; width: 90px;
        -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
    }
    .onoffswitch-checkbox {
        position: absolute;
        opacity: 0;
        pointer-events: none;
    }
    .onoffswitch-label {
        display: block; overflow: hidden; cursor: pointer;
        border: 2px solid #999999; border-radius: 20px;
    }
    .onoffswitch-inner {
        display: block; width: 200%; margin-left: -100%;
        transition: margin 0.3s ease-in 0s;
    }
    .onoffswitch-inner:before, .onoffswitch-inner:after {
        display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
        font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
        box-sizing: border-box;
    }
    .onoffswitch-inner:before {
        content: "Live";
        padding-left: 10px;
        background-color: #0da612; color: #FFFFFF;
    }
    .onoffswitch-inner:after {
        content: "Demo";
        padding-right: 10px;
        background-color: dimgrey; color: linen;
        text-align: right;
    }
    .onoffswitch-switch {
        display: block;
        width: 18px;
        background: #FFFFFF;
        position: absolute;
        top: 0;
        bottom: 0;
        border: 2px solid #999999; border-radius: 20px;
        transition: all 0.3s ease-in 0s;

        margin-bottom: 5px;
        margin-top: 3px;
        margin-right: 6px;
        margin-left: 6px;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
        margin-left: 0;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
        right: 0px;
</style>
@endsection
@section('scripts')
<script>
    $.ajax({
        url: "{{ route('admin.settings.demo') }}",
        method: "GET",
        data: {},
        success: function(response){
            $('#stripe_secret_key').val(response.data[1].value);
            $('#stripe_publishable_key').val(response.data[0].value);
            $('#type').val('demo');
            var type = 'demo';
            $.ajax({
                url: "{{ route('admin.stripemode.update') }}",
                method: "GET",
                data: {type:type},
                success: function(response){
                }
            });
        },
        error: function(response){
           console.log(response);
        }
    });

    function cardselect()
    {
        if($('.stripe_check').is(":checked")) {
            $('.success_alert').hide();
            $.ajax({
                url: "{{ route('admin.settings.live') }}",
                method: "GET",
                data: {},
                success: function(response){
                    $('#stripe_secret_key').val(response.data[1].value);
                    $('#stripe_publishable_key').val(response.data[0].value);
                    $('#type').val('live');
                    var type = 'live';
                    $.ajax({
                        url: "{{ route('admin.stripemode.update') }}",
                        method: "GET",
                        data: {type:type},
                        success: function(response){
                        }
                    });
                },
                error: function(response){
                   console.log(response);
                }
            });
        } else {
            $('.success_alert').hide();
            $.ajax({
                url: "{{ route('admin.settings.demo') }}",
                method: "GET",
                data: {},
                success: function(response){
                    $('#stripe_secret_key').val(response.data[1].value);
                    $('#stripe_publishable_key').val(response.data[0].value);
                    $('#type').val('demo');
                    var type = 'demo';
                    $.ajax({
                        url: "{{ route('admin.stripemode.update') }}",
                        method: "GET",
                        data: {type:type},
                        success: function(response){
                        }
                    });
                },
                error: function(response){
                   console.log(response);
                }
            });
        }
    }

    $('#stripe_field').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var type = $('#type').val();
        var stripe_secret_key = $('#stripe_secret_key').val();
        var stripe_publishable_key = $('#stripe_publishable_key').val();
        $.ajax({
            url: "{{ route('admin.settings.payment.update') }}",
            method:"POST",
            data:{ type: type, stripe_secret_key: stripe_secret_key, stripe_publishable_key: stripe_publishable_key },
            success: function(response){
                $('#stripe_secret_key').val(response.data[1].value);
                $('#stripe_publishable_key').val(response.data[0].value);
                $('.success_alert').show();
                $('.success_msg').text(response.message);
            },
            error: function(response){
                console.log(response);
            }

        });
    });

</script>
@endsection