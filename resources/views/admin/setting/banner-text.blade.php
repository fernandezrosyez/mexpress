@extends('admin.layout.base')

@section('title', 'Banner Text')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>First Banner Text</h5>

            @foreach($bannerText as $banner)
            @if($banner->key == 'web_banner_text_one')
            <div className="row">
                <form action="{{ route('admin.settings.banner-text.update',$banner->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor">{{$banner->value}}</textarea>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Second Banner Text</h5>

            @foreach($bannerText as $banner)
            @if($banner->key == 'web_banner_text_two')
            <div className="row">
                <form action="{{ route('admin.settings.banner-text.update',$banner->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor1">{{$banner->value}}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Third Banner Text</h5>

            @foreach($bannerText as $banner)
            @if($banner->key == 'web_banner_text_three')
            <div className="row">
                <form action="{{ route('admin.settings.banner-text.update',$banner->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor2">{{$banner->value}}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('myeditor');
    CKEDITOR.replace('myeditor1');
    CKEDITOR.replace('myeditor2');
</script>
@endsection