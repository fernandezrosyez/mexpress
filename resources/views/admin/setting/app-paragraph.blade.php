@extends('admin.layout.base')

@section('title', 'App Paragraph')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>App Paragraph</h5>

            <div className="row">
                <form action="{{ route('admin.settings.app-paragraph.update',$appParagraph->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor">{{$appParagraph->value}}</textarea>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('myeditor');
</script>
@endsection