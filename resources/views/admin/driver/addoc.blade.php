@extends('admin.layout.base')

@section('title', 'Add Document')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Add Documents</h5>
            <a href="{{ route('admin.driver.index') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> Back</a>
            <table class="table table-striped table-bordered dataTable" id="table-settings">
                <thead>
                    <tr>
                        <th>Document Name</th>
                        <th>Document Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($documents['VehicleDocuments'] as $index => $document)
                    <tr>
                        <td>
                            {{$document->name}}
                        </td>
                        <td>
                            {{$document->driver_document_status}}
                           
                        </td>
                        <td style="line-height: 34px;">
                            <a href="{{ route('admin.driver.upload.document',[$id,$document->id]) }}" class="btn btn-info">Upload</a>
                        </td>
                    </tr>
                @endforeach

                @foreach($documents['DriverDocuments'] as $index => $document)
                    <tr>
                        <td>
                            {{$document->name}}
                        </td>
                        <td>
                             {{$document->driver_document_status}}
                            
                        </td>
                        <td style="line-height: 34px;">
                            <a href="{{ route('admin.driver.upload.document',[$id,$document->id]) }}" class="btn btn-info">Upload</a>
                        </td>
                    </tr>
                @endforeach

                @foreach($documents['VehicleImage'] as $index => $document)
                    <tr>
                        <td>
                            {{$document->name}}
                        </td>
                        <td>
                             {{$document->driver_document_status}}
                           
                        </td>
                        <td style="line-height: 34px;">
                            <a href="{{ route('admin.driver.upload.document',[$id,$document->id]) }}" class="btn btn-info">Upload</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                    <tr>
                       <th>Document Name</th>
                        <th>Document Status</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$(document).ready(function(){
    $('#table-settings').DataTable( {
        responsive: true,
        dom: 'Bfrtip',
        buttons: []
    } );
});
</script>
@endsection
