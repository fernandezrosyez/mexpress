@extends('admin.layout.base')
@section('title', 'Review and Rating')
@section('content')
<script type="text/javascript" src="{{asset('asset/js/rateyo-js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/rateyo-js/jquery.rateyo.js')}}"></script>
<link rel="stylesheet" href="{{ asset('asset/js/rateyo-js/jquery.rateyo.min.css') }}"/>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.driver.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>
            <h5 class="mb-1">
                Review and Rating
            </h5> 
            <div class="row">
                <div class="col-sm-4">
                    <div class="rating-block">
                        <h4>Average driver rating</h4>
                        <h2 class="bold padding-bottom-7">{{$driver->overall_rating}} <small>/ 5</small></h2>
                        <div id="rateYo"></div> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <hr/>
                    <div class="review-block">
                    @if(count($serviceRequests)>0)
                    @foreach($serviceRequests as $key=>$serviceRequest)
                        <div class="row">
                            <div class="col-sm-3">
                                @if($serviceRequest->passenger->picture=='')
                                <img src="{{URL::asset('asset/img/profile.png')}}" class="img-rounded1" style="width: 40%;
    height: auto;
    object-fit: cover;
    border-radius: 50%;">
                                @else
                                <img src="{{$serviceRequest->passenger->picture}}" class="img-rounded1" style="width: 40%;
    height: auto;
    object-fit: cover;
    border-radius: 50%;">
                                @endif
                                <div class="review-block-name">{{$serviceRequest->passenger->first_name}} {{$serviceRequest->passenger->last_name}}</div>
                                
                                <div class="review-block-date">{{date('Y-m-d h:i A',strtotime($serviceRequest->created_at))}}</div>
                                <div class="review-block-name"><a href="
                                    {{ route('admin.request.details',[$serviceRequest->request_id,'all']) }}">{{$serviceRequest->request_no}}</a></div>
                            </div>
                            <div class="col-sm-9">
                                <div class="review-block-rate">
                                    <div id="rateYo{{$key}}"></div>
                                    <script>
                                        $(function () {
                                            $("#rateYo{{$key}}").rateYo({ 
                                                rating: {{$serviceRequest->rating_by_passenger}}, 
                                                spacing: "10px", 
                                                numStars: 5, 
                                                minValue: 0, 
                                                maxValue: 5, 
                                                normalFill: '#a6a6a6', 
                                                ratedFill: 'orange',
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="review-block-description">{{$serviceRequest->comment_by_passenger}}</div>
                            </div>
                        </div>
                        <hr/>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .img-rounded {
        width: 60%;
        height: auto;
        object-fit: cover;
    }
    body {
    padding-top: 0px;
    }
    .btn-grey{
        background-color:#D8D8D8;
        color:#FFF;
    }
    .rating-block{
        background-color:#FAFAFA;
        border:1px solid #EFEFEF;
        padding:15px 15px 20px 15px;
        border-radius:3px;
    }
    .bold{
        font-weight:700;
    }
    .padding-bottom-7{
        padding-bottom:7px;
    }

    .review-block{
        background-color:#FAFAFA;
        border:1px solid #EFEFEF;
        padding:15px;
        border-radius:3px;
        margin-bottom:15px;
    }
    .review-block-name{
        font-size:12px;
        margin:10px 0;
    }
    .review-block-date{
        font-size:12px;
    }
    .review-block-rate{
        font-size:13px;
        margin-bottom:15px;
    }
    .review-block-title{
        font-size:15px;
        font-weight:700;
        margin-bottom:10px;
    }
    .review-block-description{
        font-size:13px;
    }   


    .onoffswitch {
        position: relative; width: 90px;
        -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
    }
    .onoffswitch-checkbox {
        position: absolute;
        opacity: 0;
        pointer-events: none;
    }
    .onoffswitch-label {
        display: block; overflow: hidden; cursor: pointer;
        border: 2px solid #999999; border-radius: 20px;
    }
    .onoffswitch-inner {
        display: block; width: 200%; margin-left: -100%;
        transition: margin 0.3s ease-in 0s;
    }
    .onoffswitch-inner:before, .onoffswitch-inner:after {
        display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
        font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
        box-sizing: border-box;
    }
    .onoffswitch-inner:before {
        content: "YES";
        padding-left: 10px;
        background-color: #0da612; color: #FFFFFF;
    }
    .onoffswitch-inner:after {
        content: "NO";
        padding-right: 10px;
        background-color: dimgrey; color: linen;
        text-align: right;
    }
    .onoffswitch-switch {
        display: block;
        width: 18px;
        background: #FFFFFF;
        position: absolute;
        top: 0;
        bottom: 0;
        border: 2px solid #999999; border-radius: 20px;
        transition: all 0.3s ease-in 0s;

        margin-bottom: 5px;
        margin-top: 3px;
        margin-right: 6px;
        margin-left: 6px;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
        margin-left: 0;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
        right: 0px;
    }
</style>

@endsection
@section('scripts')
<script>
$(document).ready(function(){
    $(function () {
        $("#rateYo").rateYo({ 
            rating: {{$driver->overall_rating}}, 
            spacing: "10px", 
            numStars: 5, 
            minValue: 0, 
            maxValue: 5, 
            normalFill: '#a6a6a6', 
            ratedFill: 'orange',
        });
    });
});
</script>
@endsection