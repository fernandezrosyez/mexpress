@extends('admin.layout.base')

@section('title', 'Upload Document')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.driver.document',[$user_id]) }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Upload Document</h5>

            <form class="form-horizontal" action="{{route('admin.driver.update.document')}}" method="POST" enctype="multipart/form-data" role="form">

                {{csrf_field()}}
                {{-- <input type="hidden" name="_method" value="PATCH"> --}}

                <div class="form-group row">
                    <label for="key" class="col-xs-2 col-form-label">Document Name</label>
                    <div class="col-xs-10">
                        <input type="hidden" name="user_id" id="user_id" value="{{$user_id}}">
                        <input type="hidden" name="doc_id" id="doc_id" value="{{$doct->id}}">

                        <input class="form-control" type="text" value="{{ $doct->name }}" name="key" id="key" placeholder="Key" readonly>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="file" class="col-xs-2 col-form-label">Document</label>
                    <div class="col-xs-10">
                        <input type="file" accept="image/*" name="file" class="dropify form-control-file" id="file" aria-describedby="fileHelp">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Upload</button>
                        <a href="{{ route('admin.driver.document',[$user_id]) }}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
