<?php

namespace App\Http\Controllers\Admin\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\Model\ServiceType\MstServiceType;
use Storage;


class ServiceTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $services = MstServiceType::orderBy('created_at' , 'desc')->get();
        return view('admin.service.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'                  => 'required|max:255',
            'provider_name'         => 'required|max:255',
            'image'                 => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'capacity'              => 'required|numeric',
            'description'           => 'required',
            'fixed'                 => 'required|numeric',
            'insure_price'          => 'required|numeric',
            'min_price'             => 'required|numeric',
            'price'                 => 'required|numeric',
            'minute'                => 'required|numeric',
            'distance'              => 'required|numeric',
            'calculator'            => 'required|in:MIN,HOUR,DISTANCE,DISTANCEMIN,DISTANCEHOUR',
            'min_waiting_time'      => 'required|numeric',
            'min_waiting_charge'    => 'required|numeric',
            
        ]);

        try {
            $service = new MstServiceType;

            if(isset($request->image) && !empty($request->image)){
                $image = $request->image->store('public/service');
                $image=str_replace("public", "storage", $image);
                $service->image=$image;
            }

           
            $service->name                  = $request->name;
            $service->provider_name         = $request->provider_name;
            $service->fixed                 = $request->fixed;
            $service->distance              = $request->distance;
            $service->minute                = $request->minute;
            $service->price                 = $request->price;
            $service->capacity              = $request->capacity;
            $service->calculator            = $request->calculator;
            $service->description           = $request->description;
            $service->insure_price          = $request->insure_price;
            $service->min_price             = $request->min_price;
            $service->min_waiting_time      = $request->min_waiting_time;
            $service->min_waiting_charge    = $request->min_waiting_charge;
            $service->save();
           
            return back()->with('flash_success','Service Type Saved Successfully');
        } catch (Exception $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $service = MstServiceType::findOrFail($id);
            return view('admin.service.edit',compact('service'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }

    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'                  => 'required|max:255',
            'provider_name'         => 'required|max:255',
            'image'                 => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'capacity'              => 'required|numeric',
            'description'           => 'required',
            'fixed'                 => 'required|numeric',
            'insure_price'          => 'required|numeric',
            'min_price'             => 'required|numeric',
            'price'                 => 'required|numeric',
            'minute'                => 'required|numeric',
            'distance'              => 'required|numeric',
            'calculator'            => 'required|in:MIN,HOUR,DISTANCE,DISTANCEMIN,DISTANCEHOUR',
            'min_waiting_time'      => 'required|numeric',
            'min_waiting_charge'    => 'required|numeric',
        ]);

        try {
            $service = MstServiceType::findOrFail($id);
            if(isset($request->image) && !empty($request->image)){
                $Storage=Storage::delete($service->image);
                $image = $request->image->store('public/service/'.$id);
                $image=str_replace("public", "storage", $image);
                $service->image=$image;
            }

            $service->name = $request->name;
            $service->provider_name = $request->provider_name;
            $service->fixed = $request->fixed;
            $service->insure_price = $request->insure_price;
            $service->min_price = $request->min_price;
            $service->price = $request->price;
            $service->minute = $request->minute;
            $service->distance = $request->distance;
            $service->calculator = $request->calculator;
            $service->capacity = $request->capacity;
            $service->description = $request->description;
            $service->min_waiting_time = $request->min_waiting_time;
            $service->min_waiting_charge = $request->min_waiting_charge;
            $service->save();

            return redirect()->route('admin.service.index')->with('flash_success', 'Service Type Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }


    public function destroy($id)
    {
        try {
            MstServiceType::find($id)->delete();
            return back()->with('flash_success', 'Service Type deleted successfully');
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        } catch (Exception $e) {
            return back()->with('flash_error', 'Service Type Not Found');
        }
    }
}
