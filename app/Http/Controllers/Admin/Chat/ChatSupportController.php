<?php

namespace App\Http\Controllers\Admin\Chat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChatSupportController extends Controller
{
	public function chatSupport(){
		return view('admin.chat.index');
	}
}
