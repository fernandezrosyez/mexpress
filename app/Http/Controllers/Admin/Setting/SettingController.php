<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting\Setting;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\WebService;


class SettingController extends Controller
{
    public function bannerText(){
        $WebService=new WebService();
        $BannerText=$WebService->accessGetBannerText();
        $bannerText = $BannerText['data'];
        return view('admin.setting.banner-text',compact('bannerText'));
    }

    public function bannerTextUpdate(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'web_banner_text_one'||$site_setting->key == 'web_banner_text_two'||$site_setting->key == 'web_banner_text_three') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();
            return redirect()->route('admin.settings.banner-text')->with('flash_success', 'Banner Text Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Banner Text Not Found');
        }
    }

    public function aboutUS(){
        $WebService=new WebService();
        $AboutUS=$WebService->accessGetAboutUS();
        $aboutUS = $AboutUS['data'];
        return view('admin.setting.about-us',compact('aboutUS'));
    }

    public function aboutUsUpdate(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'web_about_us') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();
            return redirect()->route('admin.settings.about-us')->with('flash_success', 'About Us Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'About Us Not Found');
        }
    }

    public function appParagraph(){
        $WebService=new WebService();
        $AppParagraph=$WebService->accessGetAppParagraph();
        $appParagraph = $AppParagraph['data'];
        return view('admin.setting.app-paragraph',compact('appParagraph'));
    }

    public function appParagraphUpdate(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'web_app_paragraph') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();
            return redirect()->route('admin.settings.app-paragraph')->with('flash_success', 'App Paragraph Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'App Paragraph Not Found');
        }
    }

   
    public function index()
    {
        // ->orWhere('key','web_site')->orWhere('key','contact_number')->orWhere('key','contact_email')->orWhere('key','sos_number')
        $site_settings = Setting::where('key','site_title')->orWhere('key','site_logo')->orWhere('key','site_email_logo')->orWhere('key','site_icon')->orWhere('key','site_copyright')->get();
        return view('admin.setting.index', compact('site_settings'));
    }
    
    public function privacyPolicy()
    {
        $policies = Setting::where('key','condition_privacy')->orWhere('key','page_privacy')->get();
        return view('admin.setting.policy', compact('policies'));
    }

    public function privacyPolicyUpdate(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);
            if ($site_setting->key == 'page_privacy') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
                $site_setting->save();
                return redirect()->route('admin.privacy.policy')->with('flash_success', 'Privacy Policy Updated Successfully');
            }
            
            if ($site_setting->key == 'condition_privacy') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
                $site_setting->save();
                return redirect()->route('admin.privacy.policy')->with('flash_success', 'Terms & Conditions Updated Successfully');
            }
             
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Privacy Policy Not Found');
        }
    }

   
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

  
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        try {
            $site_setting = Setting::findOrFail($id);
            return view('admin.setting.edit',compact('site_setting'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    
    public function update(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);
            if ($request->key == 'site_title'||$request->key == 'site_copyright'||$site_setting->key == 'web_site'||$site_setting->key == 'contact_number'||$site_setting->key == 'contact_email'||$site_setting->key == 'sos_number') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            } else {
                $this->validate($request, [
                    'value' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                ]);
                if(isset($request->value) && !empty($request->value)){
                    //$Storage=Storage::delete($site_setting->value);
                    $value = $request->value->store('public/setting/'.$id);
                    $value=str_replace("public", "storage", $value);
                    $site_setting->value=$value;
                }
            }
            $site_setting->save();
            return redirect()->route('admin.setting.index')->with('flash_success', 'Site Setting Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    
    public function destroy($id)
    {
        //
    }

    public function paymentSetting()
    {
        return view('admin.setting.payment');
    }

    public function liveStripe()
    {
        $stripe = Setting::where('key','stripe_secret_key')->orwhere('key','stripe_publishable_key')->get();

        return response()->json([
            'success' => true,
            'message' => 'Live Result.',
            'data' => $stripe,
        ], 200);
    }

    public function demoStripe()
    {
        $stripe = Setting::where('key','stripe_secret_key_test')->orWhere('key','stripe_publishable_key_test')->get();
        
        return response()->json([
            'success' => true,
            'message' => 'Demo Result.',
            'data' => $stripe,
        ], 200);
    }

    public function updateStripeMode(Request $request)
    {
        $stripeMode = Setting::where('key','stripe_mode')->first();
        $stripeMode->value = $request->type;
        $stripeMode->save();

        return response()->json([
            'success' => true,
            'message' => 'Stripe Mode Result.',
            'data' => $stripeMode,
        ], 200);
    }

    public function updateStripe(Request $request)
    {
        if ($request->type=='live') {
            $stripe = Setting::where('key','stripe_secret_key')->orwhere('key','stripe_publishable_key')->get();
            $stripe[0]->value = $request->stripe_publishable_key;
            $stripe[1]->value = $request->stripe_secret_key;
            $stripe[0]->save();
            $stripe[1]->save();
        }
        
        if ($request->type=='demo') {
            $stripe = Setting::where('key','stripe_secret_key_test')->orWhere('key','stripe_publishable_key_test')->get();
            $stripe[0]->value = $request->stripe_publishable_key;
            $stripe[1]->value = $request->stripe_secret_key;
            $stripe[0]->save();
            $stripe[1]->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Updated Successfully.',
            'data' => $stripe,
        ], 200);
    }

}
