<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


use Validator;
class PaymentController extends Controller
{
    public function getDriverPaymentStatactics(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $db=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total  FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and sr.driver_id=?',[ $request->user_id]);

            $totalTrip=0;
            $totalEarining =0;
            $totalTransfer=0;
            $totalBalance =0;

            if($db[0]->tripecount!==0){
                $totalTrip=$db[0]->tripecount;
                $totalEarining =(float)$db[0]->total;
                $totalTransfer=0;
                $totalBalance =0;
            }
            $Background=[
                "totalTrip"=>$totalTrip,
                "totalEarining"=>$totalEarining,
                "totalTransfer"=>$totalTransfer,
                "totalBalance"=>$totalBalance,
                "currency"=>"$"
            ];
            return response(['message'=>"Driver Background","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function getDriverPaymentList(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $timeZone=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $db=DB::select('SELECT tl.transaction_log_id,CONVERT_TZ(tl.updated_at,"+00:00","'.$timeZone[0].':'.$timeZone[1].'") as updated_at,
            tl.cost,tl.currency,tl.payment_method,sr.request_no
            FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id
            where sr.payment_status="COMPLETED"  and sr.request_status="COMPLETED" and tl.transaction_type="FORRIDE" and sr.driver_id=?',[ $request->user_id]);
            return response(['message'=>"Driver Payment List","data"=>$db,"errors"=>array("exception"=>["OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }



    }
}
