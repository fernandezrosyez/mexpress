<?php

namespace App\Http\Controllers\Api\WebService;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\WebService;
use Validator;
use App\User;
use App\Notifications\NotificationByEmail;
use Illuminate\Support\Facades\Notification;


class WebController extends Controller
{
	public function getAboutUS(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $WebService=new WebService();
            $aboutUS=$WebService->accessGetAboutUS();
            return response(['message'=>"About Us.","data"=>$aboutUS['data'],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function getAppParagraph(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $WebService=new WebService();
            $AppParagraph=$WebService->accessGetAppParagraph();
            return response(['message'=>"App paragraph.","data"=>$AppParagraph['data'],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function getBannerText(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $WebService=new WebService();
            $BannerText=$WebService->accessGetBannerText();
            return response(['message'=>"Banner Text.","data"=>$BannerText['data'],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function getTermsConditions(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $WebService=new WebService();
            $termCondition=$WebService->accessTermCondition();
            return response(['message'=>"Terms & Conditions.","data"=>$termCondition['data'],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function getPrivacyPolicy(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $WebService=new WebService();
            $termCondition=$WebService->accessPrivacyPolicy();
            return response(['message'=>$termCondition['message'],"data"=>$termCondition['data'],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }


    public function contactUS(Request $request){
        try{

            $rule=[
                'response' => 'required',

            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            if (!empty($_SERVER['HTTP_CLIENT_IP']))
            {
              $ip_address = $_SERVER['HTTP_CLIENT_IP'];
            }
          //whether ip is from proxy
          elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            {
              $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
          //whether ip is from remote address
          else
            {
              $ip_address = $_SERVER['REMOTE_ADDR'];
            }
            $recaptcha = new \ReCaptcha\ReCaptcha("6LdRtNEZAAAAAM_6NrFcpED3DvSv59KZ7gKsCTvd");
            $resp = $recaptcha->setExpectedHostname('demos.mydevfactory.com/')
                              ->verify($request->response, $ip_address);
            if ($resp->isSuccess()) {
                $request['timeZone']=$timeZone=$request->header("timeZone");
                $rule=[
                    'first_name' => 'required|max:255',
                    'email'      => 'required|email|max:255',
                    'phone_no'   => 'required|max:10',
                    'isd_code'   => 'required',
                    'message'    => 'required',
                    'timeZone'   => 'required'
                ];
                $validator=$this->requestValidation($request->all(),$rule);
                if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

                $WebService=new WebService();
                $contact=$WebService->accessCreateContact($request);

                $msg = $contact->first_name.' '.$contact->last_name.' trying to contact you.<br><p>Contact No.: '.$contact->isd_code.'-'.$contact->phone_no.'</p><br><p>Email ID: '.$contact->email.'</p><br><p>Address: '.$contact->address.'</p><br><p>Message: '.$contact->message.'</p>';
                $sub = 'Someone Contact You';
                $user = User::where('user_scope','admin-service')->first();
                $user->email = $user->admin_profile->email_id;
                Notification::send($user, new NotificationByEmail($msg,$sub));

                return response(['message'=>"Contact us.","data"=>$contact,"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);


            } else {
                return response(['message'=>"Hey robot, you are not allowed to make this request","data"=> $resp->getErrorCodes(),"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],403);

            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

}
