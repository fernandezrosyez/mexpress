<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ServiceTypeMst;

use Validator;
class ServiceTypeMstController extends Controller
{
    public function get(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
           
            $ServiceTypeMst=new ServiceTypeMst();
            $ServiceTypeMstData=$ServiceTypeMst->accessGet();
            return response(['message'=>"Service list","data"=>$ServiceTypeMstData,"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);
            
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }
}