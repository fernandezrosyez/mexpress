<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Services\MessageService;
use App\Services\ServiceRequestService;
use App\Services\NotificationServices;
use App\Services\NotificationService;

use Validator;

class MessageController extends Controller
{
    public function getMessage(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");

            $rule=[
                'request_id'=>'required',
                'timeZone'=>'required',
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422);
            };
            $request->user_id=Auth::user()->id;
            $MessageService=new MessageService();
            $mes=$MessageService->accessGetLocation($request->request_id,$request['timeZone']);







            return response(['message'=>'message',"data"=>$mes,"errors"=>[]],(int)200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function sendMessage(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");

            $rule=[
                'request_id'=>'required',
                'timeZone'=>'required',
                'user_scope'=>'required|in:driver-service,passenger-service',
                'message'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422);
            };
            $request->user_id=Auth::user()->id;

            $MessageService=new MessageService();
            $mes=$MessageService->accessCreateMessage($request);
            // find reciver user id
            // get request id
            $ServiceRequestService = new ServiceRequestService();
            $requestDetails= $ServiceRequestService->accessGetRequestByID($request);
            $pushDate=(object)[];
            if($request->user_scope=="driver-service"){
                // find passenger id
              //  print_r($requestDetails->passenger_id);exit;
              $pushDate=[
                "title"=>"Passenger Message",
                "text"=>$request->message,
                "body"=>$request->message,
                "type"=>"CHATNORMAL",
                "request_id"=>$request->request_id,
                "user_id"=>$requestDetails->passenger_id,
                "setTo"=>'SINGLE'
                ];

            }
            else{
                // find driver
                $pushDate=[
                    "title"=>"Driver Message",
                    "text"=>$request->message,
                    "body"=>$request->message,
                    "type"=>"CHATNORMAL",
                    "request_id"=>$request->request_id,
                    "user_id"=>$requestDetails->driver_id,
                    "setTo"=>'SINGLE',
                    'url'=>'https://demos.mydevfactory.com/debarati/mahoney_express_web/#/messaging'
                    ];
                    //$NotificationServices->sendPushNotification((object)$pushDate);

            }
            $NotificationServices =new NotificationServices();

           //print_r($pushDate); exit;
           $Notification=new NotificationService();
           $Notification->accessCreateNotification((object)$pushDate);

            $NotificationServices->sendPushNotification((object)$pushDate);


            return response(['message'=>"Message sent!","data"=>(object)[],"errors"=>[]],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }
}
