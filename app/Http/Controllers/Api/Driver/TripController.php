<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Model\ParcelImage\ParcelImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Services\DriversProfileService;
use App\Services\EmergencyContact;
use App\Services\UserService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\DriversServiceType;
use App\Services\DriverPreferencesService;
use App\Services\ServiceRequestService;
use App\Services\UnitConvertionService;
use App\Services\RequestServices;
use App\Services\LocationService;
use App\Services\TransactionLogService;
use App\Services\ParcelImageServices;
use App\Services\PassengersProfileService;
use App\Services\NotificationServices;
use App\Services\NotificationService;



use Validator;
use Exception;

class TripController extends Controller
{
    public function reject(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
            ];
          //  echo 1; exit;
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();

            $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"ACTIVE","driver_id"=>$request->user_id],"REQUESTED");
            $RequestServices=new RequestServices();
            $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"DECLINE","driver_id"=>$request->user_id],"REQUESTED");
            if($RequestServicesReturn===false){
                return response(['message'=>"Booking request your trying to reject is not valid any more","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],204);
            }



            return response(['message'=>"You have decline, a request","data"=>(object)[],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function tripControl(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
                'tripStatus'=>'required|in:ACCEPTED,REACHED,STARTED,DROP,PAYMENT,RATING,COMPLETED',
                'payment_method'=>'required_if:tripStatus,PAYMENT|in:CARD,CASH',
                'rating'=>'required_if:tripStatus,RATING',
                'comment'=>'required_if:tripStatus,RATING|max:225',
                'location_id'=>'required_if:tripStatus ACCEPTED,REACHED,STARTED,DROP',
                'isLocation'=>'required_if:tripStatus ACCEPTED,REACHED,STARTED,DROP|in:WAYPOINTS,DESTINATION,SOURCE',
                ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();
            $RequestServices=new RequestServices();
            $ServiceRequestService=new ServiceRequestService();
            $LocationService = new LocationService();
            $NotificationServices =new NotificationServices();
            $message="";
            $status="";
            $nextStep="";
            switch($request->tripStatus){
                case "ACCEPTED":
                    //updating the driver profile
                    $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"ONRIDE","driver_id"=>$request->user_id],"requested");
                    // updating the service request log
                    $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"ONRIDE","driver_id"=>$request->user_id],"REQUESTED");
                    if($RequestServicesReturn===false){
                        return response(['message'=>"Booking request your trying to accepting is not valid any more","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],204);
                    }

                    // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"ACCEPTED","driver_id"=>$request->user_id]);
                    $message="Thank you for accepting the request, please proceed towards the pickup location.";

                    $LocationServiceReturn= $LocationService->accessUpdateLocationStartedTime((object)["request_id"=>$request->request_id,"isLocation"=>$request->isLocation,"location_id"=>$request->location_id]);

                     // push
                     $pushDate=[
                        "title"=>"Ride Accepted",
                        "text"=>"Driver has accepted your ride request. Will reach to your pickup location soon.",
                        "body"=>"Driver has accepted your ride request. Will reach to your pickup location soon.",
                        "type"=>"RIDEACCEPTED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id,
                        'url'=>'https://demos.mydevfactory.com/debarati/mahoney_express_web/#/ride'
                        ];
                        $Notification=new NotificationService();
                        $Notification->accessCreateNotification((object)$pushDate);

                        $NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email

                    $status="ACCEPTED";
                    $nextStep="REACHED";
                break;
                case "REACHED":
                    // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"REACHED","driver_id"=>$request->user_id]);
                    $LocationServiceReturn= $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"isLocation"=>$request->isLocation,"location_id"=>$request->location_id]);
                    $message="Please wait for the customer";
                    $status="REACHED";

                    // push
                    $pushDate=[
                        "title"=>"Ride Reached",
                        "text"=>"Driver has arrived at your pickup location.",
                        "body"=>"Driver has arrived at your pickup location.",
                        "type"=>"RIDEREACHED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id,
                        'url'=>'https://demos.mydevfactory.com/debarati/mahoney_express_web/#/ride'
                        ];
                        $Notification=new NotificationService();
                        $Notification->accessCreateNotification((object)$pushDate);

                        $NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email

                        if($request->isLocation=="SOURCE")
                        $nextStep="PAYMENT";
                break;
                case "PAYMENT":
                    if($request->payment_method=="CASH"){
                      // update the trascation log table as payament completed,
                      // update the service request table to PAYMENT and mark payment completed,
                      $TransactionLogService = new TransactionLogService();
                      $TransactionLogService->updateCreditNoteStatusForRide((object)["request_id"=>$request->request_id,"payment_method"=>"CASH","status"=>"PAYMENT"]);
                      $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                      "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);

                      $message="Thank you for collecting the payment";
                      $status="PAYMENT";
                      $nextStep="STARTED";

                    }
                    else if($request->payment_method=="CARD"){
                            // update the trascation log table as payament completed,
                            // update the service request table to PAYMENT and mark payment completed,
                          //  $TransactionLogService = new TransactionLogService();
                           // $TransactionLogService->updateCreditNoteStatusForRide((object)["request_id"=>$request->request_id,"status"=>"PAYMENT"]);
                           // $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                           // "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                           // $message="Thank you for collecting the payment";
                           // $status="PAYMENT";
                           // $nextStep="STARTED";
                          // exit;
                          }
                    break;
                    case "STARTED":
                      //caculate waiting time;
                      $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"STARTED","driver_id"=>$request->user_id]);
                      $LocationServiceReturn= $LocationService->accessUpdateLocationStartedTime((object)["request_id"=>$request->request_id,"isLocation"=>$request->isLocation,"location_id"=>$request->location_id]);
                      $message="Please proceed towards the destination.";
                      $status="STARTED";
                      $nextStep="DROP";
                break;
                case "DROP":
                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"DROP","driver_id"=>$request->user_id]);
                    $LocationServiceReturn = $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"isLocation"=>$request->isLocation,"location_id"=>$request->location_id]);
                    $status="DROP";
                   // if($request->signature_file !=null){
                       // $retunss= $this->signatureForAndrio($request);
                       // if($retunss['status']!=201){
                         //   return response(['message'=>$retunss['message'],"data"=>(object)$retunss['data'],"errors"=>$retunss['errors']],$retunss['status']);
                      //  }
                  //  }
                  $message="You have successfully completed the delivery.";
                    switch($request->isLocation){
                          case "WAYPOINTS":
                            $nextStep="STARTED";
                            $message="You have successfully completed the delivery. Please wait while we fetch next drop location.";
                          break;
                          case "DESTINATION":
                            $nextStep="RATING";
                            $message="You have successfully completed the delivery.";
                          break;
                        }

                        // push
                        $pushDate=[
                            "title"=>"Consignment Drop",
                            "text"=>"Your consignment has been successfully delivered.",
                            "body"=>"Your consignment has been successfully delivered.",
                            "type"=>"RIDEDROP",
                            "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                            "setTo"=>'SINGLE',
                            "request_id"=>$ServiceRequestServiceReturn->request_id,
                            'url'=>'https://demos.mydevfactory.com/debarati/mahoney_express_web/#/ride'
                        ];
                        $Notification=new NotificationService();
                        $Notification->accessCreateNotification((object)$pushDate);

                        $NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email





                    break;

                case "RATING":
                    // update the service request with rating comment

                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                    "request_status"=>"RATING","rating_by_driver"=>$request->rating,"comment_by_driver"=>$request->comment,
                    "driver_rating_status"=>"GIVEN","driver_id"=>$request->user_id]);
                    $message="Thanks for reviewing the customer.";
                    $status="RATING";
                    $nextStep="COMPLETED";
                     // push
                     $pushDate=[
                        "title"=>"Delivery Completed",
                        "text"=>"Your all consignment has been successfully delivered. Thank you for choosing us.",
                        "body"=>"Your all consignment has been successfully delivered. Thank you for choosing us.",
                        "type"=>"RIDECOMPLETED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id,
                        'url'=>'https://demos.mydevfactory.com/debarati/mahoney_express_web/#/ride'
                    ];
                    $Notification=new NotificationService();
                    $Notification->accessCreateNotification((object)$pushDate);

                    $NotificationServices->sendPushNotification((object)$pushDate);
                    // send sms
                    $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                    // email
                break;
                case "COMPLETED":
                    $message="Thank you for completing the ride safely. ";
                    $status="COMPLETED";
                    $nextStep="NORMAL";
                break;
            }
            return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }

    public function signature(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'location_id'=>'required',
                'signature_file' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $LocationService=new LocationService();

            if($request->hasFile('signature_file')) {
                $upload_signature_path =$request->signature_file->store('public/driver/parcel/'.$request->location_id.'/signature');
                $upload_signature_path= str_replace("public", "storage", $upload_signature_path);
                $request->signaturePath=$upload_signature_path;
                $LocationService->accessUpdateLocationSignature($request);
            }
            else{
                return response(['message'=>"File Not Found.","data"=>(object)[],"errors"=>array("exception"=>["Decline"],"e"=>[])],404);
            }
            return response(['message'=>"Signature uploaded.","data"=>(object)[],"errors"=>array("exception"=>["Upladed"],"e"=>[])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function createParcelImage(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'location_id'=>'required',
                'images' => 'required|mimes:jpeg,jpg,bmp,png|max:5242880',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->driver_id=Auth::user()->id;
            $ParcelImageServices=new ParcelImageServices();

            if($request->hasFile('images')) {
                $upload_images_path =$request->images->store('public/driver/parcel/'.$request->location_id.'/images');
                $upload_images_path= str_replace("public", "storage",  $upload_images_path);
                $request->images=$upload_images_path;
                $ParcelImageServices->accesscreateImage($request);
            }
            else{
                return ['message'=>"File Not Found.","data"=>(object)[],"errors"=>array("exception"=>["Decline"],"e"=>[]),"status"=>204];
            }
            return ['message'=>"file uploaded.","data"=>(object)["imageList"=>$ParcelImageServices->accessgetAllImage($request)],"errors"=>array("exception"=>["Upladed"],"e"=>[]),"status"=>201];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"status"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e),"status"=>400];

        }
    }
    public function getParcleImage(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'location_id'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->driver_id=Auth::user()->id;
            $ParcelImageServices=new ParcelImageServices();
            return ['message'=>"parcel image list.","data"=>(object)["imageList"=>$ParcelImageServices->accessgetAllImage($request)],"errors"=>array("exception"=>["Upladed"],"e"=>[]),"status"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"status"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e),"status"=>400];
        }
    }
    public function tripHistory(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $ParcelImageServices=new ParcelImageServices();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByDriverId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['schedule_datetime']=$this->checkNull($requestBooking["data"][$key]['schedule_datetime']);
                if($requestBooking["data"][$key]['schedule_datetime']!=""){
                    $requestBooking["data"][$key]['schedule_datetime']=$this->convertFromUTC($requestBooking["data"][$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['started_from_source']=$this->checkNull($requestBooking["data"][$key]['started_from_source']);
                if($requestBooking["data"][$key]['started_from_source']!=""){
                    $requestBooking["data"][$key]['started_from_source']=$this->convertFromUTC($requestBooking["data"][$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking["data"][$key]['dropped_on_destination']=$this->checkNull($requestBooking["data"][$key]['dropped_on_destination']);
                if($requestBooking["data"][$key]['dropped_on_destination']!=""){
                    $requestBooking["data"][$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking["data"][$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking["data"][$key]['accepted_on']=$this->checkNull($requestBooking["data"][$key]['accepted_on']);
                if($requestBooking["data"][$key]['accepted_on']!=""){
                    $requestBooking["data"][$key]['accepted_on']=$this->convertFromUTC($requestBooking["data"][$key]['accepted_on'],$request->timeZone);
                }

                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking["data"][$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>0,"descriptions"=>"","recipientPh"=>"","isdCode"=>"","recipientName"=>"","isSignature"=>"","signaturePath"=>"","parcelImage"=>[] ];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],
                            "address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking["data"][$key]['request_id']]);
                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];
                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }

                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');

                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking["data"][$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];
            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>["Everything is OK."])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function tripDetails(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $PassengersProfileService=new PassengersProfileService();
            $ParcelImageServices=new ParcelImageServices();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByDriverId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            foreach($requestBooking as $key=>$val){
                $requestBooking[$key]['schedule_datetime']=$this->checkNull($requestBooking[$key]['schedule_datetime']);
                if($requestBooking[$key]['schedule_datetime']!=""){
                    $requestBooking[$key]['schedule_datetime']=$this->convertFromUTC($requestBooking[$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking[$key]['started_from_source']=$this->checkNull($requestBooking[$key]['started_from_source']);
                if($requestBooking[$key]['started_from_source']!=""){
                    $requestBooking[$key]['started_from_source']=$this->convertFromUTC($requestBooking[$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking[$key]['dropped_on_destination']=$this->checkNull($requestBooking[$key]['dropped_on_destination']);
                if($requestBooking[$key]['dropped_on_destination']!=""){
                    $requestBooking[$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking[$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking[$key]['accepted_on']=$this->checkNull($requestBooking[$key]['accepted_on']);
                if($requestBooking[$key]['accepted_on']!=""){
                    $requestBooking[$key]['accepted_on']=$this->convertFromUTC($requestBooking[$key]['accepted_on'],$request->timeZone);
                }

                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking[$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>0,"descriptions"=>"","recipientPh"=>"","isdCode"=>"","recipientName"=>"","isSignature"=>"","signaturePath"=>"","parcelImage"=>[] ];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],
                            "address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking[$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking[$key]['request_id']]);
                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];

                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }

                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');

                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking[$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];

                $ProfileData=$PassengersProfileService->accessGetProfile((object)['user_id'=>$requestBooking[$key]['passenger_id']]);

                $myBooking[$key]['ProfileData']=$ProfileData['data'];
            }
            $requestBooking= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking[0],"errors"=>array("exception"=>["Everything is OK."])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
}

