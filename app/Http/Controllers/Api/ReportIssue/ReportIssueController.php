<?php
namespace App\Http\Controllers\Api\ReportIssue;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\ReportIssue\ReportIssue;
use App\Model\ReportIssue\ReportSubject;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Storage;


class ReportIssueController extends Controller{
    public function reportIssue(Request $request){
        try{
            $request['timeZone']=$request->header("timeZone");
            $rule=[
                'subject' => 'required',
                'description'=>'required',
                'reportBy'=>'required|in:Driver,Customer',
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            $ReportIssue=new ReportIssue();
            $ReportIssue->subject=$request->subject;
            $ReportIssue->description=$request->description;
            $ReportIssue->reportBy=$request->reportBy;
            $ReportIssue->user_id=Auth::user()->id;
            $ReportIssue->save();


            if($ReportIssue->issues_id>0){
                return response(['message'=>"Your issue has been reported. We will contact you soon","data"=>(object)[],"errors"=>array("exception"=>["Return Expection"],"e"=>[])],201);
            }
            else{
                return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Return Expection"],"e"=>[])],400);
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"],"e"=>$e)],401);
        }
        catch(\Exception $e){
            // return response()->json(['error' => $e->getMessage()]);
            return response(['message'=>'Something went wrong',"errors"=>array("exception"=>["Exception"],"e"=>$e)],500);
        }
    }

    public function getSubject(Request $request){
        try{
            $request['timeZone']=$request->header("timeZone");
            $rule=[
                'timeZone' => 'required',
                'reportBy'=>'required|in:Driver,Customer'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            $ReportIssue= ReportSubject::select("report_subject_id","subject")->where("type",$request->reportBy)->get();

            return response(['message'=>"Done","data"=>(object)$ReportIssue,"errors"=>array("exception"=>["Everything is OK."])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"],"e"=>$e)],401);
        }
        catch(\Exception $e){
            // return response()->json(['error' => $e->getMessage()]);
            return response(['message'=>'Something went wrong',"errors"=>array("exception"=>["Exception"],"e"=>$e)],500);
        }
    }

}
