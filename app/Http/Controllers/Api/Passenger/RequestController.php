<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\EstimatedFareService;
use App\Services\ServiceRequestService;
use App\Services\LocationService;
use App\Services\RequestServices;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\NotificationServices;
use App\Services\TransactionLogService;
use App\Services\DriversProfileService;
use App\Services\DriversServiceType;
use App\Services\ParcelImageServices;
use App\Services\NotificationService;

use Validator;



class RequestController extends Controller
{
    private function generateStaticMap($s_latitude,$s_longitude,$d_latitude,$d_longitude,$waypoint){
        try{
            $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$s_latitude.",".$s_longitude."&destination=".$d_latitude.",".$d_longitude."&mode=driving&key=".env('GOOGLE_MAP_KEY');

            $json = $this->curl($details);
            $details = json_decode($json, TRUE);
            //print_r($details);
            $route_key = $details['routes'][0]['overview_polyline']['points'];
            $map_icon_start = asset('asset/img/marker-start.png');
            $map_icon_end = asset('asset/img/marker-end.png');
            $wayMarker="";
                 // waypoint
            foreach($waypoint as $key=>$val){
                $wayMarker=$wayMarker."&markers=icon:".$map_icon_start."%7C".$val["latitude"].",".$val["longitude"];

                }


            $static_map = "https://maps.googleapis.com/maps/api/staticmap?".
            "autoscale=1".
            "&size=640x260".
            "&maptype=terrain".
            "&format=png".
            "&visual_refresh=true".
            "&markers=icon:".$map_icon_start."%7C".$s_latitude.",".$s_longitude.trim($wayMarker).
            "&markers=icon:".$map_icon_end."%7C".$d_latitude.",".$d_longitude.
            "&path=color:0x000000|weight:2|enc:".$route_key.
            "&key=".env('GOOGLE_MAP_KEY');
            return ["static_map"=>$static_map,"route_key"=>$route_key];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }

    }
    public function estimated(request $request){
        try{
            $rule=[
                'source' =>'required',
                'destination' => 'required',
                'waypoint' => 'sometimes',
                'preferences' => 'sometimes',
                'request_type'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request->request_status="SERVICESEARCH";
            $request->locationDetails=["source"=>$request->source,"waypoint"=>$request->waypoint,"destination"=>$request->destination];

            $staticMap=$this->generateStaticMap($request->source["latitude"],$request->source["longitude"],$request->destination["latitude"],$request->destination["longitude"],$request->waypoint);
            $request->static_map=$staticMap['static_map'];
            $request->route_key=$staticMap['route_key'];
            $ServiceRequest=new ServiceRequestService();
            $ServiceRequestResult=$ServiceRequest->accessCreateRequest($request);//echo 2; exit;
            $request->request_id=$ServiceRequestResult->request_id;
            $request->request_no="MAHEX"."/".date("ymd")."/".$request->request_id;
            $ServiceRequestResult=$ServiceRequest->accessUpdateRequestNo($request);

            /** Save the requested locations */

            $LocationService = new LocationService();
            $locationArray=[];
            // sources
            $locationArray[]=array(
                "request_id"=>$request->request_id,
                "longitude"=>$request->source["longitude"],
                "latitude"=>$request->source["latitude"],
                "address"=>$request->source["address"],
                "types"=>"source",
                "quantity"=>0,
                "descriptions"=>null,
                "recipientPh"=>null,
                "isdCode"=>null,
                "recipientName"=>null,
                "isSignature"=>"N",
                "orders"=>$request->source["order"]
            );
            // waypoint
            foreach($request->waypoint as $key=>$val){
                $locationArray[]=array(
                    "request_id"=>$request->request_id,
                    "longitude"=>$val["longitude"],
                    "latitude"=>$val["latitude"],
                    "address"=>$val["address"],
                    "types"=>"waypoint",
                    "quantity"=>$val["parcel_details"]['quantity'],
                    "descriptions"=>$val["parcel_details"]['descriptions'],
                    "recipientPh"=>$val["parcel_details"]['recipientPh'],
                    "isdCode"=>$val["parcel_details"]['isdCode'],
                    "recipientName"=>$val["parcel_details"]['recipientName'],
                    "isSignature"=>$val["parcel_details"]['isSignature'],
                    "orders"=>$val["order"]
                );
            }
            // destination
            $locationArray[]=array(
                "request_id"=>$request->request_id,
                "longitude"=>$request->destination["longitude"],
                "latitude"=>$request->destination["latitude"],
                "address"=>$request->destination["address"],
                "types"=>"destination",
                "quantity"=>$request->destination["parcel_details"]['quantity'],
                "descriptions"=>$request->destination["parcel_details"]['descriptions'],
                "recipientPh"=>$request->destination["parcel_details"]['recipientPh'],
                "isdCode"=>$request->destination["parcel_details"]['isdCode'],
                "recipientName"=>$request->destination["parcel_details"]['recipientName'],
                "isSignature"=>$request->destination["parcel_details"]['isSignature'],
                "orders"=>$request->destination["order"]
            );
            $LocationServiceReturn=$LocationService->accessCreateLocation($locationArray);
            $request->locationDetails=$locationArray;
            $EstimatedFareService =new EstimatedFareService();
            $retunEsti=$EstimatedFareService->accessGetFare($request);

           // print_r($retunEsti['message']);

            //accessGetFare
            return response(['message'=>$retunEsti['message'],"data"=>$retunEsti['data'],"errors"=>$retunEsti['errors']],$retunEsti['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function requestRide(Request $request){
        try{
            $rule=[
                'request_id' =>'required|exists:service_requests,request_id',
                'service_type_id' => 'required',
                'payment_method'=>'required|in:CASH,CARD',
                'card_id'=>'required_if:payment_method,CARD',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request->request_status="RIDESEARCH";
            $ServiceRequest=new ServiceRequestService();
            // get the data from request table
            $getRequestData=$ServiceRequest->accessGetRequestByID($request);

          //  print_r($getRequestData); exit;
            if($getRequestData->request_type=="RIDENOW"){
                switch ($getRequestData->request_status) {
                    case "SERVICESEARCH":
                        $result=$ServiceRequest->accessUpdateRideStatus($request);
                        $ServiceRequest->accessUpdatePayment($request);
                        $request_status=$request->request_status;
                        $message="Thank you for choosing us, we are connecting with near by drivers.";
                    break;
                    case "RIDESEARCH":
                        $request_status=$getRequestData->request_status;
                        $message="We are connecting with near by drivers, we appreciate your patience.";
                    break;
                    }
            }
            return response(['message'=>$message,"data"=>(object)["request_status"=>$request_status,"request_id"=>$request->request_id],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function ratingComment(Request $request){
        try{
            $rule=[
                'request_id' =>'required',
                'rating'=>'required',
                'comment'=>'required|max:225'
            ];
            $validator=$this->requestValidation($request->all(),$rule);

            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->passenger_id=Auth::user()->id;
            $ServiceRequestService=new ServiceRequestService();

            $ServiceRequestServiceReturn = $ServiceRequestService->accessAddPassengerRatingComment((object)[
                "request_id"=>$request->request_id,
                "rating_by_passenger"=>$request->rating,
                "comment_by_passenger"=>$request->comment,
                "passenger_rating_status"=>"GIVEN",
                "passenger_id"=>$request->user_id
                ]);
            // calculate over all ratting.



            $message="Thanks for reviewing the driver.";
            $status="RATING";
            $nextStep="COMPLETED";

            return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["ok"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function scheduleRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $request['ApplicationType']=$timeZone=$request->header("ApplicationType");
            $rule=[
                'request_id' =>'required|exists:service_requests,request_id',
                'service_type_id' => 'required',
                'payment_method'=>'required|in:CASH,CARD',
                'card_id'=>'required_if:payment_method,CARD',
                'date'=>'required',
                'time'=>'required',
               // 'comment'=>'sometimes',
                'timeZone'=>'required',
                'ApplicationType'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $message="";
            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request_status= $request->request_status="SERVICESEARCH";

            Log::info("json_encode(request)");
            Log::info(json_encode($request->date));
            Log::info(json_encode($request->time));
            Log::info("json_encode(request)");

            //exit;
            $ServiceRequest=new ServiceRequestService();
            // get the data from request table
            $getRequestData=$ServiceRequest->accessGetRequestByID($request);
            //  print_r($getRequestData); exit;
            if($getRequestData->request_type=="RIDENOW"){
                // echo $getRequestData->request_status;
                // echo "sd"; exit;
                switch ($getRequestData->request_status) {
                    case "SERVICESEARCH":
                        $getRequestData->request_type="SCHEDULE";
                        $result=$ServiceRequest->accessUpdateRideStatus($request);
                       // $result=$ServiceRequest->accessUpdateRideStatus($request);
                        $ServiceRequest->accessUpdatePayment($request);
                        //  $ServiceRequest->accessUpdatePayment($request);
                        $result->request_type=$getRequestData->request_type;
                        // save schedule date time in utc
                        // check its from web or not




                        $result->schedule_datetime=$this->convertToUTC($request->date." ".$request->time,$request['timeZone']);
                        Log::info("result->schedule_datetime");
                        Log::info(json_encode($result->schedule_datetime));
                        Log::info("result->schedule_datetime".$request['timeZone']);



                        $result->save();
                        $request_status=$request->request_status;
                        $message="Thank you for choosing us, nearby drivers will contact you on your scheduled time.";
                    break;
                }
            }
            return response(['message'=>$message,"data"=>(object)["request_status"=>$request_status,"request_id"=>$request->request_id],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],200);


        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }
    public function scheduleCancel(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $request['ApplicationType']=$timeZone=$request->header("ApplicationType");
            $rule=[
                'request_id' =>'required|exists:service_requests,request_id',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $message="";
            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request_status= $request->request_status="CANCELBYPASSENGER";
            $ServiceRequest=new ServiceRequestService();
            // get the data from request table
            $getRequestData=$ServiceRequest->accessGetRequestByID($request);
            //  print_r($getRequestData); exit;
            if($getRequestData->request_type=="SCHEDULE"){
                switch ($getRequestData->request_status) {
                    case "SERVICESEARCH":
                        $getRequestData->request_type="SCHEDULE";
                        $result=$ServiceRequest->accessUpdateRideStatus($request);
                        $result->request_type=$getRequestData->request_type;
                        $request_status=$request->request_status;
                        $message="Your Schedule is been canceled successfully.";
                    break;
                }
            }
            return response(['message'=>$message,"data"=>(object)["request_status"=>$request_status,"request_id"=>$request->request_id],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }

    public function sendScheduleRideNotification(){
        try{
            $minutes_to_add = 1;
            $currentDateTime = date("Y-m-d H:i",strtotime("+10 minutes",strtotime(date('Y-m-d H:i'))));
            $addedOneMin = new \DateTime($currentDateTime);
            $addedOneMin->add(new \DateInterval('PT' . $minutes_to_add . 'M'));
            $addedOneMin = $addedOneMin->format('Y-m-d H:i');
            Log::info("between ".$currentDateTime." and".$addedOneMin);
            $RequestServices = new RequestServices();
            $ServiceRequest=new ServiceRequestService();
            $NotificationServices =new NotificationServices();
            // find all the schedule ride
            $allSchedule= $RequestServices->accessGetAllActiveSchedule((object)["currentDateTime"=>$currentDateTime,"addedOneMin"=>$addedOneMin]);
            // print_r($allSchedule); exit;
             Log::info(json_encode($allSchedule));
            foreach($allSchedule as $key=>$val){
                if($val['request_type']=="SCHEDULE"){
                    // Log::info("I am in the schedule blog");
                    switch ($val['request_status']) {
                        case "SERVICESEARCH":
                            Log::info("I am in the SERVICESEARCH blog");
                            $request=['request_status'=>"RIDESEARCH",'request_id'=>$val['request_id'],"passenger_id"=>$val['passenger_id']];
                            // check for RIDESEARCH ACCEPTED REACHED STARTED DROP PAYMENT RATING
                            $isOnRide=$ServiceRequest->accessCheckOnRide((object)$request);
                            if(!empty($isOnRide)&&count($isOnRide)>0){
                                //cancel the schedule ride
                                $result=$ServiceRequest->accessAutoCancelSchedule((object)$request);
                                // push
                                $pushDate=[
                                    "title"=>"Scheduled Ride Cancel",
                                    "text"=>"Your scheduled ride which is due in next 10 minutes, has been cancelled by us as you are already on a ride.",
                                    "body"=>"Your scheduled ride which is due in next 10 minutes, has been cancelled by us as you are already on a ride.",
                                    "type"=>"SCHEDULEDRIDECANCEL",
                                    "user_id"=>$val['passenger_id'],
                                    "setTo"=>'SINGLE',
                                    "request_id"=>$val['request_id'],
                                    'url'=>'https://demos.mydevfactory.com/debarati/mahoney_express_web/#/ride'
                                    ];
                                    $Notification=new NotificationService();
                                    $Notification->accessCreateNotification((object)$pushDate);

                                    $NotificationServices->sendPushNotification((object)$pushDate);
                                    // send sms
                                    $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                                    // email
                            }
                            else{
                                $result=$ServiceRequest->accessUpdateRideStatusSetSchedule((object)$request);
                            }
                        break;
                    }
                }
            }
            return true;
        }
        catch(\Illuminate\Database\QueryException  $e){
            Log::info(json_encode($e));
            return false;
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            Log::info(json_encode($e));
            return false;
        }
        catch(ModelNotFoundException $e){
            Log::info(json_encode($e));
            return false;
        }
        catch (\Exception $e){
            Log::info(json_encode($e));
            return false;
        }
    }

    public function tripHistory(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $ParcelImageServices=new ParcelImageServices();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByPassengerId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['schedule_datetime']=$this->checkNull($requestBooking["data"][$key]['schedule_datetime']);
                if($requestBooking["data"][$key]['schedule_datetime']!=""){
                    $requestBooking["data"][$key]['schedule_datetime']=$this->convertFromUTC($requestBooking["data"][$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['started_from_source']=$this->checkNull($requestBooking["data"][$key]['started_from_source']);
                if($requestBooking["data"][$key]['started_from_source']!=""){
                    $requestBooking["data"][$key]['started_from_source']=$this->convertFromUTC($requestBooking["data"][$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking["data"][$key]['dropped_on_destination']=$this->checkNull($requestBooking["data"][$key]['dropped_on_destination']);
                if($requestBooking["data"][$key]['dropped_on_destination']!=""){
                    $requestBooking["data"][$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking["data"][$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking["data"][$key]['accepted_on']=$this->checkNull($requestBooking["data"][$key]['accepted_on']);
                if($requestBooking["data"][$key]['accepted_on']!=""){
                    $requestBooking["data"][$key]['accepted_on']=$this->convertFromUTC($requestBooking["data"][$key]['accepted_on'],$request->timeZone);
                }

                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking["data"][$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>0,"descriptions"=>"","recipientPh"=>"","isdCode"=>"","recipientName"=>"","isSignature"=>"","signaturePath"=>"","parcelImage"=>[] ];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],
                            "address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking["data"][$key]['request_id']]);
                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];
                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }

                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');

                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking["data"][$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];



            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>["Everything is OK."])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }

    }

    public function upCommingScheduleTrip(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $requestBooking=$RequestServices->accessGetAllUpComingSchuduleRequestByPassengerId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            $DriversServiceType = new DriversServiceType();
            $EstimatedFareService=new EstimatedFareService();
            $ParcelImageServices=new ParcelImageServices();
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['schedule_datetime']=$this->checkNull($requestBooking["data"][$key]['schedule_datetime']);
                if($requestBooking["data"][$key]['schedule_datetime']!=""){
                    $requestBooking["data"][$key]['schedule_datetime']=$this->convertFromUTC($requestBooking["data"][$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['started_from_source']=$this->checkNull($requestBooking["data"][$key]['started_from_source']);
                if($requestBooking["data"][$key]['started_from_source']!=""){
                    $requestBooking["data"][$key]['started_from_source']=$this->convertFromUTC($requestBooking["data"][$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking["data"][$key]['dropped_on_destination']=$this->checkNull($requestBooking["data"][$key]['dropped_on_destination']);
                if($requestBooking["data"][$key]['dropped_on_destination']!=""){
                    $requestBooking["data"][$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking["data"][$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking["data"][$key]['accepted_on']=$this->checkNull($requestBooking["data"][$key]['accepted_on']);
                if($requestBooking["data"][$key]['accepted_on']!=""){
                    $requestBooking["data"][$key]['accepted_on']=$this->convertFromUTC($requestBooking["data"][$key]['accepted_on'],$request->timeZone);
                }

                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking["data"][$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>0,"descriptions"=>"","recipientPh"=>"","isdCode"=>"","recipientName"=>"","isSignature"=>"","signaturePath"=>"","parcelImage"=>[] ];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],
                            "address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];

                $driverSer=$DriversServiceType->getServiceForScheduleHistory((object)['service_type_id'=>$requestBooking["data"][$key]['service_type_id']]);
               $myBooking[$key]['driverSer']=$driverSer['data'];
              // $tracationsReturn= $EstimatedFareService->getEstimatedFare((object)["request_id"=>$requestBooking["data"][$key]['request_id']]);
               //print_r((object)["request_id"=>$requestBooking["data"][$key]['request_id']]); exit;



            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>["Everything is OK."])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }

    }

    public function tripDetails(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $DriversProfileService=new DriversProfileService();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByPassengerId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            $ParcelImageServices=new ParcelImageServices();
            foreach($requestBooking as $key=>$val){
               // print_r($requestBooking); exit;
                $requestBooking[$key]['schedule_datetime']=$this->checkNull($requestBooking[$key]['schedule_datetime']);
                if($requestBooking[$key]['schedule_datetime']!=""){
                    $requestBooking[$key]['schedule_datetime']=$this->convertFromUTC($requestBooking[$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking[$key]['started_from_source']=$this->checkNull($requestBooking[$key]['started_from_source']);
                if($requestBooking[$key]['started_from_source']!=""){
                    $requestBooking[$key]['started_from_source']=$this->convertFromUTC($requestBooking[$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking[$key]['dropped_on_destination']=$this->checkNull($requestBooking[$key]['dropped_on_destination']);
                if($requestBooking[$key]['dropped_on_destination']!=""){
                    $requestBooking[$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking[$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking[$key]['accepted_on']=$this->checkNull($requestBooking[$key]['accepted_on']);
                if($requestBooking[$key]['accepted_on']!=""){
                    $requestBooking[$key]['accepted_on']=$this->convertFromUTC($requestBooking[$key]['accepted_on'],$request->timeZone);
                }

                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking[$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>0,"descriptions"=>"","recipientPh"=>"","isdCode"=>"","recipientName"=>"","isSignature"=>"","signaturePath"=>"","parcelImage"=>[] ];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],
                            "address"=>$locVal['address'],"orders"=>$locVal['orders'],
                            "quantity"=>$locVal['quantity'],"descriptions"=>$locVal['descriptions'],"recipientPh"=>$locVal['recipientPh'],
                            "isdCode"=>$locVal['isdCode'],"recipientName"=>$locVal['recipientName'],"isSignature"=>$locVal['isSignature'],
                            "signaturePath"=>$this->appendBaseurlToImagePath($locVal['signaturePath']),"parcelImage"=>$ParcelImageServices->accessgetAllImage((object)["request_id"=>$locVal['request_id'],"location_id"=>$locVal['location_id']])
                        ];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking[$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking[$key]['request_id']]);
                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];
                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }

                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');




                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking[$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];
                $ProfileData=$DriversProfileService->accessGetProfile((object)['user_id'=>$requestBooking[$key]['driver_id']]);

                $myBooking[$key]['ProfileData']=$ProfileData['data'];


            }
            $requestBooking= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking[0],"errors"=>array("exception"=>["Everything is OK."])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }

    }

}
