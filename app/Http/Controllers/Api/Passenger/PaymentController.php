<?php
namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\PassengersProfileService;
use App\Services\UsersDevices;
use App\Services\StripePaymentService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\ServiceRequestService;
use App\Services\EstimatedFareService;
use App\Services\TransactionLogService;

use Validator;

class PaymentController extends Controller
{
    public function addCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'card_token'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            // get customer id
            $PassengersProfileService =new PassengersProfileService();
            $profileData=$PassengersProfileService->accessGetProfile($request);

            $StripePaymentService=new StripePaymentService();
            $request->stripe_customer_id=$profileData['data']->stripe_customer_id;

            if($profileData['data']->stripe_customer_id==null || $profileData['data']->stripe_customer_id=="" ){
                // create customer
                $CustomerID=$StripePaymentService->accessCreateCustomer($profileData['data']);
                $request->stripe_customer_id=$CustomerID['stripe_cust_id'];
                // update the customer id in the passenger profile
                $PassengersProfileService->accessUpdateCustomerID($request);
            }

            // Add Cards
            $card=$StripePaymentService->accessAddCards($request);
            return response(['message'=>$card['message'],"data"=>$card['data'],"errors"=>$card['errors']],$card['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function ListCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            $StripePaymentService=new StripePaymentService();

            $card=$StripePaymentService->accessListCard($request);

            return response(['message'=>$card['message'],"data"=>$card['data'],"errors"=>$card['errors']],$card['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function DeleteCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'card_token' => 'required|exists:user_cards,card_token,user_id,'.Auth::user()->id
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;

            $StripePaymentService=new StripePaymentService();
            $PassengersProfileService =new PassengersProfileService();

            $profileData=$PassengersProfileService->accessGetProfile($request);
            $request->stripe_customer_id=$profileData['data']->stripe_customer_id;

            $card=$StripePaymentService->accessDeleteCard($request);

            return response(['message'=>$card['message'],"data"=>$card['data'],"errors"=>$card['errors']],$card['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }


    }

    public function setDefaultCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[ 'timeZone'=>'required','card_token' => 'required'];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;
            $StripePaymentService=new StripePaymentService();
            $card=$StripePaymentService->accessSetDefaultCard($request);

            return response(['message'=>"Default card updated","data"=>[],"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }

    public function makeCardPayment(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id' =>'required|exists:service_requests,request_id'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->user_id=Auth::user()->id;
            $request->driver_id=null;
            $request->request_status="RIDESEARCH";
            $ServiceRequest=new ServiceRequestService();
            $StripePaymentService=new StripePaymentService();
            $EstimatedFareService=new EstimatedFareService();
            $PassengersProfileService =new PassengersProfileService();
            $TransactionLogService=new TransactionLogService();
            $transcationID=0;$message="";$responsDate=[];$responsCode=400;

            // get request details
            $getRequestData=$ServiceRequest->accessGetRequestByID($request);
            $request->driver_id=$getRequestData->driver_id;
            // get profile data
            $profileData=$PassengersProfileService->accessGetProfile($request);


            // check for request status
            if($getRequestData->request_type=="RIDENOW" || $getRequestData->request_type=="SCHEDULE"){
                switch ($getRequestData->request_status) {
                    case "REACHED":
                        // get estimated fare and card details
                        $paymentObject=(object)['cost'=>0,'card_id'=>"","trans_id"=>"","stripe_customer_id"=>"","description"=>"","payment_method"=>"CARD","request_id"=>$request->request_id];

                        $retunEsti=$EstimatedFareService->getEstimatedFare($request);
                        $transcation=$TransactionLogService->getCreditNoteForRide($request);

                        $paymentObject->cost=(float)$retunEsti->estimated_cost-(float)$transcation['data']['TransactionLog']->promo_code_value;
                        $paymentObject->card_id=$getRequestData->card_id;
                        $paymentObject->stripe_customer_id=$profileData["data"]->stripe_customer_id;
                        $paymentObject->description="Request No: ".$getRequestData->request_no;
                        // check for card is given is valid or not
                        // check for card is have valid balance

                        if((float)$paymentObject->cost>0){
                            // charge the cards
                            $payment=$StripePaymentService->accessCreateStripeCharge($paymentObject);
                            //  dd($payment); exit;
                            if($payment['status']===1 ||$payment['status']===true){
                                $paymentObject->trans_id=$payment['trans_id'];
                                $transcation =$TransactionLogService->updateCreditNoteStatusForRide($paymentObject);
                                // dd($transcation); exit;
                                if($transcation['statusCode']==200){
                                    $transcationID=$transcation['data']['transaction_log_id'];
                                    $ServiceRequestServiceReturn = $ServiceRequest->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                    "request_status"=>"PAYMENT","payment_method"=>"CARD","payment_status"=>"COMPLETED","driver_id"=>$request->driver_id]);
                                    //    print_r($ServiceRequestServiceReturn);
                                    $message="Thank you for payment, now your consignment will get picked up by driver.";
                                    $responsDate=["transcationID"=>$transcationID];
                                    $responsCode=200;
                                }
                                else{
                                    $message="Transcation failed";$responsDate=[];$responsCode=400;
                                }
                            }
                            else{
                                $message="Transcation failed";$responsDate=[];$responsCode=400;
                            }
                        }
                        else{
                            $paymentObject->trans_id=$transcation['data']['TransactionLog']->promo_code;
                            $transcation =$TransactionLogService->updateCreditNoteStatusForRide($paymentObject);
                            if($transcation['statusCode']==200){
                                $transcationID=$transcation['data']['transaction_log_id'];
                                $ServiceRequestServiceReturn = $ServiceRequest->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                "request_status"=>"PAYMENT","payment_method"=>"CARD","payment_status"=>"COMPLETED","driver_id"=>$request->driver_id]);
                                //    print_r($ServiceRequestServiceReturn);
                                $message="Thank you for payment, now your consignment will get picked up by driver.";
                                $responsDate=["transcationID"=>$transcationID];
                                $responsCode=200;
                            }
                            else{
                                $message="Transcation failed";$responsDate=[];$responsCode=400;
                            }
                        }
                        //  print_r($payment); exit;
                    break;
                }
            }
            else{
                $message="You dont have access to this page";$responsDate=[];$responsCode=403;
            }
            return response(['message'=>$message,"data"=>$responsDate,"errors"=>array("exception"=>["DataBase Excetion"],"e"=>[])],$responsCode);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }

}
