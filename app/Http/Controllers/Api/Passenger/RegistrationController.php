<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\PassengersProfileService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\EmailService;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Services\OtpVerifyService;


class RegistrationController extends Controller
{
    public function register(Request $request){
        try{
             $request['timeZone']=$timeZone=$request->header("timeZone");
             
             $rule=['social_unique_id' => ['required_if:login_by,facebook,google','unique:users'],
               'device_type' => 'required|in:web,android,ios',
               'device_token' => 'required',
               'device_id' => 'required',
               'login_by' => 'required|in:manual,facebook,google',
               'first_name' => 'required|max:255',
               'last_name' => 'required|max:255',
               'email_id' => 'required|email|max:255|unique:passengers_profile',
               'mobile_no' => 'required|max:10|unique:passengers_profile',
               'isdCode' => 'required',
               'password' => 'required_if:login_by,manual|between:6,255|confirmed',
               'timeZone'=>'required',
               'isMobileActive'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
             //======= verify OTP ==================
             if($request->isMobileActive!==1){
                return response(['message'=>"Please verify your phone number.","data"=>(object)[],"errors"=>array("exception"=>["SMS Gateway error"],"e"=>[])],403);
             }
             
             //-======================
            $request->user_scope="passenger-service";
            $request->username=$request->mobile_no;
            $User=new UserService();
            $Profile=new PassengersProfileService();
            $UserDevice=new UsersDevices();
            //$EmergencyContact=new EmergencyContact();
            

            $ProfileData=(object)[];
            $DevicesData=(object)[];
            //$EmergencyContactData=[];
            $UserData=$User->accessCreateUser($request);

            if((int)$UserData->id>0){
                $request->user_id=(int)$UserData->id;
                $ProfileData=$Profile->accessCreateProfile($request);
                $DevicesData=$UserDevice->accessCreateDevices($request);
                //$EmergencyContactData=$EmergencyContact->accessGetContact($request);         
            }
            if(!empty($UserData)){
                if($UserData->id>0){
                    //autologin to the system.
                    $UserAuth=new AuthService();
                    $UserToken=$UserAuth->accessAutoLogin((object)['user_id'=>$UserData->id,"user_scope"=>$request->user_scope]);
                    if($UserToken['statusCode']==200){
                        //send email
                        // $user = Auth::user();
                        // $link = "https://laravel.com/docs/7.x/notifications";
                        // $user->email=$user->passenger_profile->email_id;
                        // $email_service=new EmailService();
                        // $email_service->accessSendEmail($user,$link);
                       
                        $UserAccessToken=$UserToken['data']->access_token;
                        $token_type=$UserToken['data']->token_type;
                        return response(['message'=>"Thank you for registering with us!","data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData,$request->login_by)],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],201);
                    }
                    else{
                        return response(['message'=>$UserToken['message'],"data"=>$UserToken['data'],"errors"=>$UserToken['errors']],$UserToken['statusCode']);
                    }
                }
            }
            return response(['message'=>"Registration not possible. Contact Administrator.","data"=>(object)[],"errors"=>array("exception"=>["Not Found"],"e"=>[])],404);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
    }

    public function onlyOTP(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'mobile_no' => 'required|max:10|unique:passengers_profile',
                'isdCode' => 'required',
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };
            $Profile=new PassengersProfileService();
            //======= send OTP using==================
            $ProfileRetun=$Profile->accessSendOTPFirst($request);
            if($ProfileRetun['statusCode']!==201){
                return response(['message'=>"Please profide valid phone number.","data"=>(object)[],"errors"=>array("exception"=>["SMS Gateway error"],"e"=>$ProfileRetun)],400);
            }
            return response(['message'=>"OTP sent successfully!","data"=>(object)["otp"=>$ProfileRetun['data']->otp],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
        
    }


    public function verifyOTP(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'mobile_no' => 'required|max:10|unique:passengers_profile',
                'isdCode' => 'required',
                'timeZone'=>'required',
                'otp'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $otpVerify=new OtpVerifyService();
            $otpVerifyRetun=$otpVerify->accessVerifyOTP($request);

            if($otpVerifyRetun['statusCode']==200){
                return response(['message'=>"OTP is verified successfully.","data"=>(object)[],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);
            }

            if($otpVerifyRetun['statusCode']==410){
                return response(['message'=>"OTP is expired.","data"=>(object)[],"errors"=>array("exception"=>["Bad Request."],"e"=>[])],410);
            }

            return response(["message"=>"OTP is not verified.","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"e"=>[])],401);
            
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e)],400);
        }
        
    }
    
}
