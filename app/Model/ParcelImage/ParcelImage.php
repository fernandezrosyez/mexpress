<?php

namespace App\Model\ParcelImage;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;


class ParcelImage extends Model
{
   	//use SoftDeletes;

       protected $table = 'parcel_image';
       protected $primaryKey = 'parcel_image_id';
       protected $hidden = [
        'created_at', 'updated_at'
    ];

}
