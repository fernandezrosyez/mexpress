<?php

namespace App\Model\ReportIssue;

use Illuminate\Database\Eloquent\Model;

class ReportSubject extends Model
{
    protected $table = 'report_issue_subject';
    protected $primaryKey = 'report_subject_id';
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
