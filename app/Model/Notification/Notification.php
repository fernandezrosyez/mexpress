<?php

namespace App\Model\Notification;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notification_table';
    protected $primaryKey = 'notification_table_id';


}
