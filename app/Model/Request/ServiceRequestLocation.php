<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;

class ServiceRequestLocation extends Model
{
  
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_request_locations';
    protected $primaryKey = 'location_id';

    public function parcelImage()
    {
        return $this->hasMany('App\Model\ParcelImage\ParcelImage', 'location_id', 'location_id');
    } 

}
