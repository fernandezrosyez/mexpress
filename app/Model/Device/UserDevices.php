<?php

namespace App\Model\Device;

use Illuminate\Database\Eloquent\Model;

class UserDevices extends Model
{
    //
   // use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_devices';
    protected $primaryKey = 'device_id';
}
