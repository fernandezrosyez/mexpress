<?php

namespace App\Model\Transaction;

use Illuminate\Database\Eloquent\Model;

class TransactionLog extends Model
{
    //
   // use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = [
        'request_id',
        'ride_insurance',
        'per_minute',
        'per_distance_km',
        'minimum_waiting_time_in_minutes',
        'waiting_charge_per_min',
        'waitTime',
        'duration_hr','duration_min', 'duration_sec',
        'duration','distance_km','distance_miles','distance_meters','cost',
        'payment_method','payment_gateway_charge','payment_gateway_transaction_id','types','transaction_type','is_paid','promo_code','promo_code_value',
        'updated_at','created_at','status'
    ];
    protected $table = 'transaction_log';
    protected $primaryKey = 'transaction_log_id';
}
