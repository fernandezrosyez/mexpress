<?php

namespace App\Services;

use App\Notifications\EmailNotification;
use Illuminate\Support\Facades\Notification;


class EmailService
{
	private function sendEmail($user,$link){

		Notification::send($user, new EmailNotification($link));

	}
	
	public function accessSendEmail($user,$link){
        return $this->sendEmail($user,$link);
    }
}