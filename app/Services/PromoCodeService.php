<?php

namespace App\Services;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\Model\Promocode\Promocode;
use App\Model\Promocode\PromocodeUsage;



class PromoCodeService
{
    // check promocde for the experi date

    private function CheckPromoCodeExperyDateBeforeAdd($data){
        $getPromo=Promocode::where("promo_code",$data->promo_code)->whereRaw("expiration >= STR_TO_DATE(?, '%Y-%m-%d %H:%i:%s')",[date("Y-m-d H:i:s")])->get()->toArray();
        if(count($getPromo)>0){
            return ["status"=>"true", "data"=>$getPromo];
        }
        return ["status"=>"false", "data"=>[]];
    }
    private function UpdatePromoCodeToUsed($data){
        $PromocodeUsage=PromocodeUsage::where("promocode_usages_id",$data->promocode_usages_id)->first();
        $PromocodeUsage->status="USED";
        return $PromocodeUsage->save();
    }

    private function UsePromocode($data){
        $dt=$this->CheckPromoCodeExperyDateBeforeAdd($data);
        $return=["status"=>400, "promocode_usage_id"=>0,"message"=>"Promo code does not exit"];

        if($dt['status']=="true"){
            // check code is already in used!
            $checkCode=PromocodeUsage::where("promocode_id",$dt['data'][0]['promocodes_id'])->where("user_id",$data->user_id)->get()->toArray();
            if(count($checkCode)>0){
                $return=["status"=>422, "promocode_usage_id"=>0,"message"=>"The code you entered has already been redeemed."];
            }
            else{
                $PromocodeUsage=new PromocodeUsage();
                $PromocodeUsage->user_id=$data->user_id;
                $PromocodeUsage->promocode_id=$dt['data'][0]['promocodes_id'];
                $PromocodeUsage->status="ADDED";
                $PromocodeUsage->save();
                $return=["status"=>200, "promocode_usage_id"=>$PromocodeUsage->promocode_usages_id,"message"=>"Promo code added"];
            }
        }
        return $return;
    }
    private function FindPromoCode($data){
        // find passenger id from request id
        $getPromo=DB::select('SELECT p.promo_code,p.discount,p.expiration,p.status,pu.promocode_usages_id FROM service_requests sr
        left join promocode_usages pu on pu.user_id=sr.passenger_id and pu.status="ADDED"
        left join promocodes p on DATE(p.expiration)>=STR_TO_DATE(?, "%Y-%m-%d")
        where p.promocodes_id=pu.promocode_id and sr.request_id=? limit 1',[date("Y-m-d H:i:s"), $data->request_id]);
        if(count($getPromo)>0){
            $return=["status"=>200, "getPromo"=>$getPromo,"message"=>"Promo code"];
        }
        else{
            $return=["status"=>404, "getPromo"=>$getPromo,"message"=>"No promo code"];
        }

        return $return;
    }
    private function GetPromocodeList($data){
        $getPromo=DB::select("select p.promo_code,p.discount,p.expiration,pu.status,pu.promocode_usages_id,p.currency
        from promocodes p
        left join promocode_usages pu on pu.promocode_id=p.promocodes_id and pu.status='ADDED' and pu.user_id=? where pu.user_id=?",[$data->user_id,$data->user_id]);
        $return=["status"=>200, "getPromo"=>$getPromo,"message"=>"Promo code"];
        return $return;
    }

    public function accessUsePromocode($data){
      return  $this->UsePromocode($data);
    }

    public function accessGetPromocodeList($data){

        return  $this->GetPromocodeList($data);
    }

    public function accesFindPromoCode($data){
        return $this->FindPromoCode($data);
    }

    public function accesUpdatePromoCodeToUsed($data){
        return $this->UpdatePromoCodeToUsed($data);
    }



}
