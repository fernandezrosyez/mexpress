<?php

namespace App\Services;

use App\Model\Otp\Otp;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\User;


class OtpVerifyService
{
    private function verifyOTP($data){
        try{
            $otpObj = Otp::where('mobile_no',$data->mobile_no)->where('isd_code',$data->isdCode)->where('otp',$data->otp)->where('isVerified',0)->first();
            
            if(isset($otpObj)){
                if($otpObj->created_at>date('Y-m-d H:i:s', strtotime("-5 min"))){
                    $otpObj->isVerified = 1;
                    $otpObj->save();
                    return ['message'=>"OTP is verified successfully.","data"=>(object)[],"errors"=>array("exception"=>["Everything is Ok."],"error"=>[]),"statusCode"=>200];
                } else {
                    return ['message'=>"OTP is expired.","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>410];
                }
            }

            return ['message'=>"OTP is not verified.","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>401];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function verifyOTPForgetPasswordP($data){
        try{
            $otpObj = User::where('user_scope','passenger-service')->where('username',$data->mobile_no)->where('otp',$data->otp)->first();
            
            if(isset($otpObj)){
                if($otpObj->otp_created_on>date('Y-m-d H:i:s', strtotime("-5 min"))){
                    return ['message'=>"OTP is verified successfully.","data"=>(object)[],"errors"=>array("exception"=>["Everything is Ok."],"error"=>[]),"statusCode"=>200];
                } else {
                    return ['message'=>"OTP is expired.","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>410];
                }
            }

            return ['message'=>"OTP is not verified.","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>401];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function verifyOTPForgetPasswordD($data){
        try{
            $otpObj = User::where('user_scope','driver-service')->where('username',$data->mobile_no)->where('otp',$data->otp)->first();
            if(isset($otpObj)){
                if($otpObj->otp_created_on>date('Y-m-d H:i:s', strtotime("-5 min"))){
                    return ['message'=>"OTP is verified successfully.","data"=>(object)[],"errors"=>array("exception"=>["Everything is Ok."],"error"=>[]),"statusCode"=>200];
                } else {
                    return ['message'=>"OTP is expired.","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>410];
                }
            }

            return ['message'=>"OTP is not verified.","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>401];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
   

    public function accessVerifyOTP($data){
        return $this->verifyOTP($data);
    }

    public function accessVerifyOTPForgetPasswordP($data){
        return $this->verifyOTPForgetPasswordP($data);
    }

    public function accessVerifyOTPForgetPasswordD($data){
        return $this->verifyOTPForgetPasswordD($data);
    }
    
}
