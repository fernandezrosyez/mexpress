<?php
namespace App\Services;



use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Support\Facades\DB;
use App\Model\Profiles\PassengersProfile;
use App\Model\Profiles\DriverProfiles;
use App\Model\Request\ServiceRequest;
use App\Services\TwilioSMS;


class ServiceRequestService
{
      // Accessing the services
      private function createRequest($data){
          $ServiceRequest=new ServiceRequest();
          $ServiceRequest->request_no=$data->request_no;
          $ServiceRequest->passenger_id=$data->passenger_id;
          $ServiceRequest->driver_id=$data->driver_id;
          $ServiceRequest->request_type=$data->request_type;
          $ServiceRequest->request_status=$data->request_status;
          $ServiceRequest->route_key=$data->route_key;
          $ServiceRequest->static_map=$data->static_map;
          //$ServiceRequest->locationDetails=$data->locationDetails;
          //$ServiceRequest->service_preferences=$data->preferences;
          $ServiceRequest->service_type_id=0;

          $ServiceRequest->save();
          return $ServiceRequest;

      }
      private function updateRequestNo($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->request_no=$data->request_no;
        $ServiceRequest->save();
        return $ServiceRequest;
    }
    private function getRequestByID($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        return $ServiceRequest;
    }

    private function updateRideStatus($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->request_status=$data->request_status;
        $ServiceRequest->service_type_id=$data->service_type_id;
        $ServiceRequest->save();
        return $ServiceRequest;
    }
    private function updateDriver($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->request_status=$data->request_status;
        $ServiceRequest->driver_id=$data->driver_id;
        if($data->request_status==="ACCEPTED"){
            $ServiceRequest->accepted_on=date("Y-m-d H:i:s");
        }
        else if($data->request_status=="STARTED"){
            $ServiceRequest->started_from_source=date("Y-m-d H:i:s");
        }
        else if($data->request_status=="DROP"){
            $ServiceRequest->dropped_on_destination=date("Y-m-d H:i:s");
        }
        else if($data->request_status=="PAYMENT"){
            if($data->payment_method=="CASH"){
                   $ServiceRequest->payment_status=$data->payment_status;
            }
            else if($data->payment_method=="CARD"){
                $ServiceRequest->payment_status=$data->payment_status;
            }

        }
        else if($data->request_status=="RATING"){
            $ServiceRequest->rating_by_driver=$data->rating_by_driver;
            $ServiceRequest->comment_by_driver=$data->comment_by_driver;
            $ServiceRequest->driver_rating_status=$data->driver_rating_status;
        }
        $ServiceRequest->save();
        if($data->request_status=="RATING"){
            $this->calculateOveralRatingPassenger($ServiceRequest);
        }
        return $ServiceRequest;
    }
    private function calculateOveralRatingPassenger($data){
        $sql='SELECT ROUND(avg(rating_by_driver),2) as overall_rating FROM service_requests where passenger_id="'.$data->passenger_id.'" and driver_rating_status="GIVEN"';
        $overAllRating=  DB::select($sql);
        $DriverProfiles=PassengersProfile::where("user_id",$data->passenger_id)->first();
        $DriverProfiles->overall_rating=$overAllRating[0]->overall_rating;
        $DriverProfiles->save();
        return true;


    }
    private function calculateOveralRatingDriver($data){
        $sql='SELECT ROUND(avg(rating_by_passenger),2) as overall_rating FROM service_requests where driver_id="'.$data->driver_id.'" and passenger_rating_status="GIVEN"';
        $overAllRating=  DB::select($sql);
        $DriverProfiles=DriverProfiles::where("user_id",$data->driver_id)->first();
        $DriverProfiles->overall_rating=$overAllRating[0]->overall_rating;
        $DriverProfiles->save();
        return true;
    }

    private function addPassengerRatingComment($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->rating_by_passenger=$data->rating_by_passenger;
        $ServiceRequest->comment_by_passenger=$data->comment_by_passenger;
        $ServiceRequest->passenger_rating_status=$data->passenger_rating_status;
        $ServiceRequest->save();
      //  print_r($ServiceRequest);
        // update over all rating
       $this->calculateOveralRatingDriver($ServiceRequest);






        return $ServiceRequest;
    }


    private function getRiderActiveRequests($data){

    $ServiceRequest=ServiceRequest::where("passenger_id",$data->user_id)
        ->where(function($sq){
            $sq->where("request_status","<>","SERVICESEARCH")
            ->where("request_status","<>","COMPLETED")
            ->where("request_status","<>","NOSERVICEFOUND")
            ->where("passenger_rating_status","PENDING")
            ->orWhere(function($sqls){
                $sqls->where("request_status","COMPLETED")
                ->where("passenger_rating_status","PENDING");
            });
        })->get()->toArray();
       return $ServiceRequest;
    }

    private function updatePaymentMethod($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->payment_method=$data->payment_method;
        if($data->payment_method=="CARD"){
            $ServiceRequest->card_id=$data->card_id;
        }
        $ServiceRequest->save();
        return $ServiceRequest;
    }
    private function checkOnRide($data){
        // Log::info("IN the server requesrt accessUpdateRideStatusSetSchedule");Log::info(json_encode($data));
         $ServiceRequest=ServiceRequest::where("passenger_id",$data->passenger_id)
         ->whereIn('request_status', ["RIDESEARCH", "ACCEPTED", "REACHED", "STARTED", "DROP", "PAYMENT", "RATING"])->get()->toArray();
        // Log::info("IN the server requesrt accessUpdateRideStatusSetSchedule");Log::info(json_encode($ServiceRequest));
         return $ServiceRequest;
     }

     private function autoCancelSchedule($data){
         $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
         $ServiceRequest->request_status="CANCELBYSYSTEM";
         $ServiceRequest->save();
         return $ServiceRequest;
     }
     private function setSchedule($data){
        // Log::info("in setSchedule");Log::info(json_encode($data));
         $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
         $ServiceRequest->request_status=$data->request_status;
         $ServiceRequest->save();
        // Log::info("on setSchedule");Log::info(json_encode($ServiceRequest));
         return $ServiceRequest;
     }

    public function accessCreateRequest($data){
        return $this->createRequest($data);
    }
    public function accessUpdateRequestNo($data){
        return $this->updateRequestNo($data);
    }

    public function accessGetRequestByID($data){
        return $this->getRequestByID($data);
    }

    public function accessUpdateRideStatus($data){
        return $this->updateRideStatus($data);
    }

    public function accessGetRiderActiveRequests($data){
        return $this->getRiderActiveRequests($data);
    }
    public function accessUpdateDriver($data){
        return $this->updateDriver($data);
    }
    public function accessAddPassengerRatingComment($data){
        return $this->addPassengerRatingComment($data);
    }
    public function accessUpdatePayment($data){
        return $this->updatePaymentMethod($data);
    }
    public function accessCheckOnRide($data){
        return $this->checkOnRide($data);
    }

    public function accessAutoCancelSchedule($data){
        return $this->autoCancelSchedule($data);
    }

    public function accessUpdateRideStatusSetSchedule($data){
        //Log::info("IN the server requesrt accessUpdateRideStatusSetSchedule");Log::info(json_encode($data));
        $returns=$this->setSchedule($data);
        //Log::info("after setSchedule");Log::info(json_encode($returns));

        return $returns;
    }


}
