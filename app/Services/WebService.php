<?php

namespace App\Services;

use App\Model\Setting\Setting;
use App\Model\Contact\ContactUs;


class WebService
{
	private function getBannerText(){
		try{
			$bannerText = Setting::where('key','web_banner_text_one')->orwhere('key','web_banner_text_two')->orwhere('key','web_banner_text_three')->get();
			return ['message'=>"About Us.","data"=>$bannerText,"errors"=>array("exception"=>["Everything is OK"],"error"=>[]),"statusCode"=>200];
		}
		catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];            
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e),"statusCode"=>500];
        }
        catch (Exception $e) {
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
        }

	}

	private function getAboutUS(){
		try{
			$aboutUS = Setting::where('key','web_about_us')->first();
			return ['message'=>"About Us.","data"=>$aboutUS,"errors"=>array("exception"=>["Everything is OK"],"error"=>[]),"statusCode"=>200];
		}
		catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];            
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e),"statusCode"=>500];
        }
        catch (Exception $e) {
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
        }

	}

	private function getAppParagraph(){
		try{
			$appParagraph = Setting::where('key','web_app_paragraph')->first();
			return ['message'=>"App Paragraph.","data"=>$appParagraph,"errors"=>array("exception"=>["Everything is OK"],"error"=>[]),"statusCode"=>200];
		}
		catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];            
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e),"statusCode"=>500];
        }
        catch (Exception $e) {
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
        }

	}

    private function getTermCondition(){
        try{
            $termCondition = Setting::where('key','condition_privacy')->first();
            return ['message'=>"Terms & Conditions.","data"=>$termCondition,"errors"=>array("exception"=>["Everything is OK"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];            
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e),"statusCode"=>500];
        }
        catch (Exception $e) {
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
        }
    }

    private function getPrivacyPolicy(){
        try{
            $termCondition = Setting::where('key','page_privacy')->first();
            return ['message'=>"Privacy Policy.","data"=>$termCondition,"errors"=>array("exception"=>["Everything is OK"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e),"statusCode"=>500];            
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"e"=>$e),"statusCode"=>500];
        }
        catch (Exception $e) {
            return ['message'=>"Something went wrong.","data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
        }
    }

    private function createContact($data){
        $ContactUs = new ContactUs();
        $ContactUs->first_name=$data->first_name;
        $ContactUs->last_name =$data->last_name;
        $ContactUs->email=$data->email;
        $ContactUs->phone_no=$data->phone_no;
        $ContactUs->isd_code=$data->isd_code;
        $ContactUs->address=$data->address;
        $ContactUs->message=$data->message;
        $ContactUs->save();
        return $ContactUs;
    }

	public function accessGetAppParagraph(){
        return $this->getAppParagraph();
    }
    public function accessGetAboutUS(){
        return $this->getAboutUS();
    }
    public function accessGetBannerText(){
        return $this->getBannerText();
    }
    public function accessCreateContact($data){
        return $this->createContact($data);
    }
    public function accessTermCondition(){
        return $this->getTermCondition();
    }
    public function accessPrivacyPolicy(){
        return $this->getPrivacyPolicy();
    }
}