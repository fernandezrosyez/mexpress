<?php

namespace App\Services;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\Model\Message\Onride;



class MessageService
{
    private function createMessageOnRide($data){
       $Oride=new Onride();
       $Oride->user_id=$data->user_id;
       $Oride->user_scope=$data->user_scope;
       $Oride->message=$data->message;
       $Oride->request_id=$data->request_id;
       $Oride->save();
       return $Oride;
    }

    private function getMessageOnRide($request_id,$timeZone){
       // echo $timeZone."===================";
        $timeZone=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
        //print_r($timeZone);
        $messageList = DB::select('SELECT messages_onride.message_id as message_id,messages_onride.request_id as request_id
        ,messages_onride.user_id as user_id ,
        messages_onride.user_scope as user_scope,
        messages_onride.message as `message`,
        CONVERT_TZ(messages_onride.updated_at,"+00:00","'.$timeZone[0].':'.$timeZone[1].'") as updated_at,
        CONVERT_TZ(messages_onride.created_at,"+00:00","'.$timeZone[0].':'.$timeZone[1].'") as created_at,
        (CASE
            WHEN messages_onride.user_scope="passenger-service"
            THEN
            passengers_profile.first_name
         ELSE
            drivers_profile.first_name
        END) as fname,
        (CASE
            WHEN messages_onride.user_scope="passenger-service"
            THEN
            passengers_profile.last_name
         ELSE
            drivers_profile.last_name
        END) as lname,
        (CASE
            WHEN messages_onride.user_scope="passenger-service"
            THEN
            passengers_profile.picture
         ELSE
            drivers_profile.picture
        END) as picture
        from messages_onride
        LEFT JOIN passengers_profile on passengers_profile.user_id=messages_onride.user_id
        LEFT JOIN drivers_profile on drivers_profile.user_id=messages_onride.user_id
        where messages_onride.request_id=?
        ORDER BY messages_onride.message_id ASC',[$request_id]);

        return $messageList;
    }

    public function accessCreateMessage($data){
        return $this->createMessageOnRide($data);
    }
    public function accessGetLocation($request_id,$timeZone){
        return $this->getMessageOnRide($request_id,$timeZone);
    }
}
