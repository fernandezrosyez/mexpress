<?php

namespace App\Services;

use App\Model\ServiceType\MstServiceType;
use App\Model\Setting\Setting;
use App\Model\Payment\UserCard;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StripePaymentService
{
    private $stripe;
    public function __construct(){
        $Setting=Setting::where("key","stripe_mode")->select("value")->first();
        if($Setting->value="demo"){
           $stripe_secret_key=Setting::where("key","stripe_secret_key_test")->select("value")->first();
        }
       else{
           $stripe_secret_key=Setting::where("key","stripe_secret_key")->select("value")->first();
        }
        $this->stripe = \Stripe\Stripe::setApiKey($stripe_secret_key->value);
    }

    private function createCustomer($data){
        try{
            $customer = \Stripe\Customer::create([
                'email' => $data->email_id,
                'name' => $data->first_name." ".$data->last_name
                ]);
                return ["message"=>"Stripe Account Created!!", "status"=>true,"Exception"=>false,"e"=>false,"stripe_cust_id"=>$customer['id']];
         }
         catch(\Exception $e){
             return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>true,"e"=>$e];
         }
    }

    private function addCard($data){
        try{
            $customer = \Stripe\Customer::retrieve($data->stripe_customer_id);
            //dd($customer); exit;

            if($customer->deleted==null){
                $card = $customer->sources->create(["source" => $data->card_token]);
            }
            else{

                return ['message'=>"Account not Found!!","data"=>(object)[],"errors"=>array("exception"=>["stripe Account not Found!!"],"error"=>[]),"statusCode"=>404];
            }

           $exist=UserCard::where('user_id',$data->user_id)
           ->where('last_four',$card['card']['last4'])
           ->where('brand',$card['card']['brand'])
           ->count();
        //   print_r($card->id); exit;
           if($exist == 0){
               $create_card = new UserCard;
               $create_card->user_id = $data->user_id;
               $create_card->card_token = $card->id;
               $create_card->last_four = $card->last4;
               $create_card->brand = $card->brand;
               $create_card->is_default =0;
               $create_card->save();
            }else{

                return ['message'=>"Card Already Added","data"=>[],"errors"=>array("exception"=>["Card Already Added"],"error"=>[]),"statusCode"=>400];
            }
            // set default card
            $findCardAll =UserCard::where("user_id",$data->user_id)->get()->toArray();
            if(count($findCardAll)===1){
                $create_card->is_default =1;
                $create_card->save();
            }
            return ['message'=>"Card Added","data"=>$create_card,"errors"=>array("exception"=>["Resource Created"],"error"=>[]),"statusCode"=>201];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to add card!","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to add card!","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Cannot able to add card!","data"=>(object)[],"errors"=>array("exception"=>["stripe resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }

    private function listCard($data){
        try{
            $cards=UserCard::where('user_id',$data->user_id)->orderBy('created_at','desc')->get()->toArray();
            if(!empty($cards))
            return ['message'=>"Card List","data"=>$cards,"errors"=>array("exception"=>["card found"],"error"=>[]),"statusCode"=>200];
            else
            return ['message'=>"No card found. Please add card.","data"=>[],"errors"=>array("exception"=>["card resource not found"],"error"=>[]),"statusCode"=>204];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to fetch your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to fetch your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found. Please add card.","data"=>(object)[],"errors"=>array("exception"=>["card resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }


    private function deleteCard($data){
        try{
            $customer = \Stripe\Customer::retrieve($data->stripe_customer_id);
            $customer->sources->retrieve($data->card_token)->delete();
            UserCard::where('card_token',$data->card_token)->delete();
            return ['message'=>"Card Deleted","data"=>[],"errors"=>array("exception"=>["card deleted"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to delete your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to delete your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found.","data"=>(object)[],"errors"=>array("exception"=>["card resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }
    private function createStripeCharge($data){
        try{
            $Charge = \Stripe\Charge::create(array(
                "amount" => $data->cost*100,
                "currency" => "usd",
                "customer" => $data->stripe_customer_id,
                "card" => $data->card_id,
                "description" => $data->description,
                "receipt_email" => "sourodipta.guha@brainiuminfotech.com"
                  ));
            return ["message"=>"Stripe Charge Created!!", "status"=>true,"Exception"=>false,"e"=>false,"trans_id"=>$Charge['id']];
        }
         catch(\Exception $e){
             return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>true,"e"=>$e];
         }
    }

    private function setDefaultCard($data){
        try{

            $UserCardUnSetDefault=UserCard::where('user_id', $data->user_id)->update(array('is_default' => 0));
            $UserCardSetDefault=UserCard::where('card_token', $data->card_token)->update(array('is_default' => 1));

            return response(['message'=>"Default card updated","data"=>[],"errors"=>array("exception"=>["Card Updated"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found.","data"=>(object)[],"errors"=>array("exception"=>["No card found."],"error"=>$e),"statusCode"=>404];
        }
    }


    public function accessCreateCustomer($data){
        return $this->createCustomer($data);
    }

    public function accessAddCards($data){
        return $this->addCard($data);
    }

    public function accessListCard($data){
        return $this->listCard($data);
    }

    public function accessDeleteCard($data){
        return $this->deleteCard($data);
    }

    public function accessSetDefaultCard($data){
     return $this->setDefaultCard($data);
    }

    public function accessCreateStripeCharge($data){
        return $this->createStripeCharge($data);
    }
}



