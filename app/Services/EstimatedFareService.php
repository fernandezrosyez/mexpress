<?php
namespace App\Services;



use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\UsersDevices;
use Illuminate\Support\Facades\DB;
use App\Model\Profiles\PassengersProfile;
use App\Model\Profiles\DriverProfiles;
use App\Model\Request\EstimatedFare;
use App\Services\TwilioSMS;
use App\Services\UserService;
use App\Services\ServiceTypeMst;
use App\Services\TransactionLogService;
use App\Services\PromoCodeService;


class EstimatedFareService
{
    private function curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close ($ch);
        return ["data"=>$return,"httpcode"=>$httpcode];
    }
   public function metersToMiles($meters){
        $number=$meters * 0.000621371;
        return number_format((float)$number, 2, '.', '');
    }
   public function metersToKilometers($meters){
       $number=$meters * 0.001;
       return number_format((float)$number, 2, '.', '');
    }
    public function fareCalucation($distance,$duration,$waitTime,$perDistanceInKmCost,$perMinuteCost,$insurence,$perMinWaitCost,$minimumWaitTime){
        $costPerKm=($this->metersToKilometers($distance)*(float)$perDistanceInKmCost);
        $costPerSecond=(float)$duration*($perMinuteCost/60);
        $waitTimeCost=0;
        if(($waitTime-$minimumWaitTime)>$minimumWaitTime){
            $waitTimeCost=($waitTime-$minimumWaitTime)*$perMinWaitCost;
        }
        $total=$costPerKm+$costPerSecond+$insurence+$waitTimeCost+2;
        return number_format((float)$total, 2, '.', '');
    }
    public function breakUPs($distance,$duration,$waitTime,$perDistanceInKmCost,$perMinuteCost,$insurence,$perMinWaitCost,$minimumWaitTime,$promo_code,$promo_code_value){
        $costPerKm=($this->metersToKilometers($distance)*(float)$perDistanceInKmCost);
        $costPerSecond=(float)$duration*($perMinuteCost/60);
        $waitTimeCost=0;
        if(($waitTime-$minimumWaitTime)>$minimumWaitTime){
            $waitTimeCost=($waitTime-$minimumWaitTime)*$perMinWaitCost;
        }
        $total=$costPerKm+$costPerSecond+$insurence+$waitTimeCost+2-$promo_code_value;
        if((float)$total<=0){
            $total=0.00;
        }

        return [
            "distanceFare"=>number_format((float)($costPerKm+$costPerSecond), 2, '.', ''),
            "waitTimeCost"=>number_format((float)$waitTimeCost, 2, '.', ''),
            "waitTimeInMin"=>0,
            "basefare"=>number_format(2, 2, '.', ''),
            "tax"=>number_format((float)($insurence), 2, '.', ''),
            "promo_code_value"=>number_format((float)($promo_code_value), 2, '.', ''),
            "promo_code"=>$promo_code,
            "total"=>number_format((float)($total), 2, '.', '')
          ];
    }
    public function toGetDuration($seconds){
        $init = $seconds;
        $hours = floor($init / 3600);
        $minutes = floor(($init / 60) % 60);
        $seconds = $init % 60;
        return ["hours"=>$hours,"minutes"=>$minutes,"seconds"=>$seconds];
    }

    private function insertEstimatedFare($data){
      //  print_r($data); exit;
        $insertEstimatedFare=new EstimatedFare();
        $insertEstimatedFare->request_id=$data->request_id;
        $insertEstimatedFare->service_type_id=$data->service_type_id;
        $insertEstimatedFare->ride_insurance=$data->ride_insurance;
        $insertEstimatedFare->per_minute=$data->per_minute;
        $insertEstimatedFare->per_distance_km=$data->per_distance_km;
        $insertEstimatedFare->minimum_waiting_time_in_minutes=$data->minimum_waiting_time_in_minutes;
        $insertEstimatedFare->waiting_charge_per_min=$data->waiting_charge_per_min;
        $insertEstimatedFare->waitTime=$data->waitTime;
        $insertEstimatedFare->estimated_duration_hr=$data->estimated_duration_hr;
        $insertEstimatedFare->estimated_duration_min=$data->estimated_duration_min;
        $insertEstimatedFare->estimated_duration_sec=$data->estimated_duration_sec;
        $insertEstimatedFare->estimated_duration=$data->estimated_duration;
        $insertEstimatedFare->estimated_distance_km=$data->estimated_distance_km;
        $insertEstimatedFare->estimated_distance_miles=$data->estimated_distance_miles;
        $insertEstimatedFare->estimated_distance_meters=$data->estimated_distance_meters;
        $insertEstimatedFare->estimated_cost=$data->estimated_cost;
        $insertEstimatedFare->currency=$data->currency;
        return $insertEstimatedFare->save();
    }

    private function getFare($services,$data){
        try{
            //  Google MAp to get the estimated time and distances
            $distance=0;
            $duration=0;
            $waitTime=0;
            $sourceLatitude="";
            $sourceLongitude="";
            $destinationLatitude="";
            $destinationLongitude="";
            $wayPointLatLog="";

            foreach($data->locationDetails as $key=>$val){
                switch($val['types']){
                    case "source":
                        $sourceLatitude=$val['latitude'];
                        $sourceLongitude=$val['longitude'];
                    break;
                    case "destination":
                        $destinationLatitude=$val['latitude'];
                        $destinationLongitude=$val['longitude'];
                    break;
                    case "waypoint":
                        if($wayPointLatLog===""){
                            $wayPointLatLog="&waypoints=".$val['latitude'].','.$val['longitude'];
                          }
                          else{
                            $wayPointLatLog=$wayPointLatLog."|".$val['latitude'].','.$val['longitude'];
                          }
                    break;
                }
            }

            if($wayPointLatLog!==""){
                $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$sourceLatitude.",".$sourceLongitude."&destination=".$destinationLatitude.",".$destinationLongitude.$wayPointLatLog."&mode=driving&key=".env('GOOGLE_MAP_KEY');
            }
            else{
                $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$sourceLatitude.",".$sourceLongitude."&destination=".$destinationLatitude.",".$destinationLongitude."&mode=driving&key=".env('GOOGLE_MAP_KEY');
            }
            $json = $this->curl($details);
            if($json['httpcode']==200){
                $details = json_decode($json['data'], TRUE);
            }
           // print_r($details); exit;

            $estimatedFareByServices=[];
            if($json['httpcode']===200){
                foreach($details['routes'] as $key){
                    foreach($key['legs'] as $abc){
                        $distance=$distance+($abc['distance']['value']);
                        $duration=$duration+($abc['duration']['value']);
                    }
                }
            }
            //echo $duration; echo "!=0 &&";echo $distance; echo "ss"; echo $this->metersToKilometers($distance);

            $durationArray=$this->toGetDuration($duration);
            $distanceInKm=$this->metersToKilometers($distance);
            $distanceInMiles=$this->metersToMiles($distance);

            if($duration!=0 && $distance!=0){
                $serviceData=(array)$services;
                foreach($serviceData as $key =>$val){
                   // print_r($val); exit;
                   $carAvaiable=$this->getServicesInRadius(10,$sourceLatitude,$sourceLongitude,$val['id']);
                    $estimatedFareByServices[]=array(
                        "name"=>$val['name'],
                        "request_id"=>$data->request_id,
                        "image"=>$val['image'],
                        "provider_name"=>$val['provider_name'],
                        "service_type_id"=>$val['id'],
                        "carDetails"=>$carAvaiable,
                        "estimated_fare"=>[
                            "ride_insurance"=>$val['insure_price'],
                            "per_minute"=>$val['minute'],
                            "per_distance_km"=>$val['distance'],
                            "minimum_waiting_time_in_minutes"=>$val['min_waiting_time'],
                            "waiting_charge_per_min"=>$val['min_waiting_charge'],
                            "waitTime"=>$waitTime,
                            "estimated_duration_hr"=> $durationArray["hours"],
                            "estimated_duration_min"=> $durationArray["minutes"],
                            "estimated_duration_sec"=> $durationArray["seconds"],
                            "estimated_duration"=>$duration,
                            "estimated_distance_km"=>$distanceInKm,
                            "estimated_distance_miles"=>$distanceInMiles,
                            "estimated_distance_meters"=>$distance,
                            "estimated_cost"=>$this->fareCalucation($distance,$duration,$waitTime,$val['distance'],$val['minute'],$val['insure_price'],$val['min_waiting_charge'],$val['min_waiting_time']),
                            "currency"=>"$"
                            ]
                        );
                    }
                }
                if(!empty($estimatedFareByServices)){
                return ['message'=>"Cars are available for your location at that given moment.","data"=>$estimatedFareByServices,"errors"=>array("exception"=>["Service found"],"error"=>[]),"statusCode"=>200];
            }
            return ['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found estimated"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    private function getServicesInRadius($radius,$sourceLatitude,$sourceLongitude,$service_trype_id,$serviceLog=[]){
        try{
            $driver_id=[];$driverSQL="";
            if(!empty($serviceLog)){
                foreach($serviceLog as $keys=>$vals){
                    $driver_id[]=$vals['driver_id'];
                }

            }
           if(!empty($driver_id)){
               $driverSQL=" and dp.user_id NOT IN(".implode(" ",$driver_id).")";
        }
           // print_r((array)$serviceTypesDetails[0]["_id"]); exit;
            $sql="SELECT dp.user_id,dp.first_name,dp.last_name,dp.mobile_no,dp.isd_code,dp.picture,dp.status,dp.service_status,dst.registration_no,
            dst.model,dst.model_year,dst.driver_service_type_id,ud.device_id,ud.device_type,ud.latitude,ud.longitude,st.name,
            st.provider_name,st.image,st.capacity,
            (1.609344 * 3956 * acos( cos( radians($sourceLatitude) ) * cos( radians(ud.latitude) ) * cos( radians(ud.longitude)-radians($sourceLongitude) ) + sin( radians($sourceLatitude) ) * sin( radians(ud.latitude) ) ) ) as eatInKmDriverFromSource


             FROM drivers_profile dp
            left join driver_service_type dst on dst.user_id=dp.user_id
            left join user_devices ud on ud.user_id=dp.user_id
            left join service_types st on st.id=dst.service_type_id
            where dst.service_type_id=$service_trype_id ".$driverSQL." and dp.status='online' and dp.service_status='ACTIVE' and ud.latitude<>'' and ud.longitude<>''
            and (1.609344 * 3956 * acos( cos( radians($sourceLatitude) ) * cos( radians(ud.latitude) ) * cos( radians(ud.longitude)-radians($sourceLongitude) ) + sin( radians($sourceLatitude) ) * sin( radians(ud.latitude) ) ) ) <$radius";


          $serviceTypesDetails=  DB::select($sql);

            if(!empty($serviceTypesDetails)){
                return ['message'=>"Cars are available for your location at that given moment.","data"=>$serviceTypesDetails,"errors"=>array("exception"=>["Service found"],"error"=>[]),"statusCode"=>200];
            }
            return ['message'=>"No cars are available for your location at that given moment.","data"=>[],"errors"=>array("exception"=>["Service not found"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    public function getFareByServiceType($data){
        try{

            $serviceTypesDetails=[];
            $servics=DB::select("service_types")->where("id",$data->service_type_id)->get();
            if(!empty($servics)){
                $serviceTypesDetails[]=$servics[0];
            }
            if(!empty($serviceTypesDetails)){
                return ['message'=>"Cars are available for your location at that given moment.","data"=>(object)$serviceTypesDetails,"errors"=>array("exception"=>["Service found"],"error"=>[]),"statusCode"=>200];
            }
            return ['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"You are not registered with us!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    public function getEstimatedFare($data){
        return EstimatedFare::where("request_id",$data->request_id)->first();
    }
    public function calculateFinalPayable($data){
        // check if create note is created or not
        $TransactionLogService=new TransactionLogService();
        $checkCreditNote=$TransactionLogService->getCreditNoteForRide($data);
        if($checkCreditNote['statusCode']==200){
            $payable_fare=[
                "request_id"=>$data->request_id,
                "ride_insurance"=>$checkCreditNote['data']['TransactionLog']->ride_insurance,
                "per_minute"=>$checkCreditNote['data']['TransactionLog']->per_minute,
                "per_distance_km"=>$checkCreditNote['data']['TransactionLog']->per_distance_km,
                "minimum_waiting_time_in_minutes"=>$checkCreditNote['data']['TransactionLog']->minimum_waiting_time_in_minutes,
                "waiting_charge_per_min"=>$checkCreditNote['data']['TransactionLog']->waiting_charge_per_min,
                "waitTime"=>(int)$checkCreditNote['data']['TransactionLog']->waitTime,
                "duration_hr"=>(int)$checkCreditNote['data']['TransactionLog']->duration_hr,
                "duration_min"=>(int)$checkCreditNote['data']['TransactionLog']->duration_min,
                "duration_sec"=>(int)$checkCreditNote['data']['TransactionLog']->duration_sec,
                "duration"=>(int)$checkCreditNote['data']['TransactionLog']->duration,
                "distance_km"=>$checkCreditNote['data']['TransactionLog']->distance_km,
                "distance_miles"=>$checkCreditNote['data']['TransactionLog']->distance_miles,
                "distance_meters"=>$checkCreditNote['data']['TransactionLog']->distance_meters,
                "cost"=>$checkCreditNote['data']['TransactionLog']->cost,
                "currency"=>$checkCreditNote['data']['TransactionLog']->currency,
                "payment_method"=>$checkCreditNote['data']['TransactionLog']->payment_method,
                "types"=>$checkCreditNote['data']['TransactionLog']->types,
                "status"=>$checkCreditNote['data']['TransactionLog']->status,
                "payment_gateway_charge"=>$checkCreditNote['data']['TransactionLog']->payment_gateway_charge,
                "payment_gateway_transaction_id"=>$checkCreditNote['data']['TransactionLog']->payment_gateway_transaction_id,
                "payment_gateway_transaction_id"=>$checkCreditNote['data']['TransactionLog']->payment_gateway_transaction_id,
                "is_paid"=>$checkCreditNote['data']['TransactionLog']->is_paid,
                "promo_code"=>$checkCreditNote['data']['TransactionLog']->promo_code,
                "promo_code_value"=>$checkCreditNote['data']['TransactionLog']->promo_code_value,
                "promocode_usages_id"=>$checkCreditNote['data']['TransactionLog']->promocode_usages_id
            ];
            $brakFare=$this->breakUPs(
                $checkCreditNote['data']['TransactionLog']->distance_meters,
                $checkCreditNote['data']['TransactionLog']->duration,
                $checkCreditNote['data']['TransactionLog']->waitTime,
                $checkCreditNote['data']['TransactionLog']->per_distance_km,
                $checkCreditNote['data']['TransactionLog']->per_minute,
                $checkCreditNote['data']['TransactionLog']->ride_insurance,
                $checkCreditNote['data']['TransactionLog']->waiting_charge_per_min,
                $checkCreditNote['data']['TransactionLog']->minimum_waiting_time_in_minutes,
                $checkCreditNote['data']['TransactionLog']->promo_code,
                $checkCreditNote['data']['TransactionLog']->promo_code_value
            );
        }
        else{
            // get the estimated fare for the
            $getEstimatedFare=$this->getEstimatedFare($data);
            //  print_r($getEstimatedFare);
            //exit;
            // check for any promocode... is added or not and is usable
            $PromoCode=new PromoCodeService();
            $useablePromoCode=$PromoCode->accesFindPromoCode($data);
            $promo_code="";
            $promo_code_value=0;
            $promocode_usages_id=0;
            if($useablePromoCode['status']==200){
                $promo_code=$useablePromoCode['getPromo'][0]->promo_code;
                $promo_code_value=$useablePromoCode['getPromo'][0]->discount;
                $promocode_usages_id=$useablePromoCode['getPromo'][0]->promocode_usages_id;
                // update promocode to used
                $PromoCode->accesUpdatePromoCodeToUsed((object)['promocode_usages_id'=>$promocode_usages_id]);
            }
            $is_paid="N";
            if($data->payment_method=="CASH"){
                $is_paid="Y";
            }
            $duration=$getEstimatedFare->estimated_duration;
            $durationArray=$this->toGetDuration($duration);
            $waitTime=0;
            $payable_fare=[
              "request_id"=>$data->request_id,
              "ride_insurance"=>$getEstimatedFare->ride_insurance,
              "per_minute"=>$getEstimatedFare->per_minute,
              "per_distance_km"=>$getEstimatedFare->per_distance_km,
              "minimum_waiting_time_in_minutes"=>$getEstimatedFare->minimum_waiting_time_in_minutes,
              "waiting_charge_per_min"=>$getEstimatedFare->waiting_charge_per_min,
              "waitTime"=>$waitTime,
              "duration_hr"=> $durationArray["hours"],
              "duration_min"=> $durationArray["minutes"],
              "duration_sec"=> $durationArray["seconds"],
              "duration"=>$duration,
              "distance_km"=>$getEstimatedFare->estimated_distance_km,
              "distance_miles"=>$getEstimatedFare->estimated_distance_miles,
              "distance_meters"=>$getEstimatedFare->estimated_distance_meters,
              "cost"=>$this->fareCalucation(
                  $getEstimatedFare->estimated_distance_meters,
                  $duration,
                  $waitTime,
                  $getEstimatedFare->per_distance_km,
                  $getEstimatedFare->per_minute,
                  $getEstimatedFare->ride_insurance,
                  $getEstimatedFare->waiting_charge_per_min,
                  $getEstimatedFare->minimum_waiting_time_in_minutes
                ),
                "currency"=>"$",
                "payment_method"=>$data->payment_method,
                "types"=>"CREDIT",
                "status"=>"PENDING",
                "is_paid"=>$is_paid,
                "promo_code"=>$promo_code,
                "promo_code_value"=>$promo_code_value,
                "promocode_usages_id"=>$promocode_usages_id
            ];

            $brakFare=$this->breakUPs(
                $getEstimatedFare->estimated_distance_meters,
                $duration,
                $waitTime,
                $getEstimatedFare->per_distance_km,
                $getEstimatedFare->per_minute,
                $getEstimatedFare->ride_insurance,
                $getEstimatedFare->waiting_charge_per_min,
                $getEstimatedFare->minimum_waiting_time_in_minutes,
                $promo_code,
                $promo_code_value
            );
    }
    // print_r($payable_fare); exit;
    if($checkCreditNote['statusCode']==200){
        $payable_fare['transaction_log_id']=$checkCreditNote['data']['transaction_log_id'];
        $payable_fare['status']=$checkCreditNote['data']['status'];
    }
    else{
        $createCreditNote= $TransactionLogService->createCreditNoteForRide($payable_fare);
        $payable_fare['transaction_log_id']=$createCreditNote->transaction_log_id;
    }
    // add break up
    $payable_fare['break_up']=$brakFare;
    return $payable_fare;
}

// Accessing the services

    public function accessGetFare($data){
        // get all service types
        $ServiceTypeMst=new ServiceTypeMst();
        $getAllServices=$ServiceTypeMst->accessGet();
        $retuns=['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found estimated"],"error"=>[]),"statusCode"=>404];
        if(!empty($getAllServices)){
            $retuns=$this->getFare($getAllServices,$data);
        }
        //print_r($getAllServices); exit;
        //  $retuns=$this->getServicesInRadius(8,$data);
        return $retuns;
    }

    public function accessGetFareByServiceType($data){
        $ServiceTypeMst=new ServiceTypeMst();
        $getAllServices=$ServiceTypeMst->accessGetNameByAllID($data);
        $retuns=['message'=>"No cars are available for your location at that given moment.","data"=>(object)[],"errors"=>array("exception"=>["Service not found estimated"],"error"=>[]),"statusCode"=>404];
        if(!empty($getAllServices)){
            $retuns=$this->getFare($getAllServices,$data);
          }
          return $retuns;

    }

    public function accessInsertEstimatedFare($data){

        return $this->insertEstimatedFare($data);

    }
  //  getServicesInRadius($radius,$sourceLatitude,$sourceLongitude,$service_trype_id){
    public function accessGetDriverWithInRadius($data,$serviceLog){
        $locationDetails=(array)$data;

        $service_type_id=$locationDetails['service_type_id'];
        foreach($locationDetails['locationDetails'] as $key=>$val){
            if($val['types']=="source"){
                $sourceLongitude=$val['longitude'];
                $sourceLatitude=$val['latitude'];
            }
        }
        return $this->getServicesInRadius(10,$sourceLatitude,$sourceLongitude,$service_type_id,$serviceLog);
    }

    public function accessGetEstimated($data){
       // print_r($data); exit;
        return $this->getEstimatedFare($data);
    }


    public function accessCalculateFinalPayable($data){
        return $this->calculateFinalPayable($data);
    }



}
