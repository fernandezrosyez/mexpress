<?php

namespace App\Services;

use App\Model\ParcelImage\ParcelImage;
use Illuminate\Support\Facades\DB;
use Storage;

class ParcelImageServices
{
    private function createImage($data){
        $ParcelImage=new ParcelImage();
        $ParcelImage->request_id=$data->request_id;
        $ParcelImage->driver_id=$data->driver_id;
        $ParcelImage->location_id=$data->location_id;
        $ParcelImage->images="https://demos.mydevfactory.com/debarati/mahoneyexpress/public/".$data->images;

        $ParcelImage->save();
        return $ParcelImage;

    }

    private function getAllImage($data){
        $ParcelImage=ParcelImage::select("parcel_image_id","location_id","images")
        ->where("request_id",$data->request_id)
        ->where("location_id",$data->location_id)->get()->toArray();
        return $ParcelImage;
    }

    public function accessgetAllImage($data){
        return $this->getAllImage($data);

    }

    public function accesscreateImage($data){
       // print_r($data); exit;
        return $this->createImage($data);
    }



}
