<?php

namespace App\Services;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\Model\Request\ServiceRequestLocation;



class LocationService
{
    private function createLocation($data){
       $ServiceRequestLocation=ServiceRequestLocation::insert($data);
       return $ServiceRequestLocation;
    }

    private function gGetLocation($request_id){
        return ServiceRequestLocation:: where("request_id",$request_id)->get()->toArray();
    }
    private function updateLocationReachedTime($data){

        $ServiceRequestLocation=ServiceRequestLocation:: where("request_id",$data->request_id)->where("location_id",$data->location_id)->first();
        $ServiceRequestLocation->reached_on=date("Y-m-d H:i:s");
        return $ServiceRequestLocation->save();
    }

    private function updateLocationStartedTime($data){
        $ServiceRequestLocation=ServiceRequestLocation:: where("request_id",$data->request_id)->where("location_id",$data->location_id)->first();
        $ServiceRequestLocation->started_on=date("Y-m-d H:i:s");
        return $ServiceRequestLocation->save();
    }

    private function updateLocationSignature($data){
        $ServiceRequestLocation=ServiceRequestLocation:: where("request_id",$data->request_id)->where("location_id",$data->location_id)->first();
        $ServiceRequestLocation->signaturePath=$data->signaturePath;
        return $ServiceRequestLocation->save();
    }


    public function accessCreateLocation($data){
        return $this->createLocation($data);
    }
    public function accessGetLocation($request_id){
        return $this->gGetLocation($request_id);
    }
    public function accessUpdateLocationReachedTime($data){
        return $this->updateLocationReachedTime($data);
    }
    public function accessUpdateLocationStartedTime($data){
        return $this->updateLocationStartedTime($data);
    }
    public function accessUpdateLocationSignature($data){
        return $this->updateLocationSignature($data);
    }


}
