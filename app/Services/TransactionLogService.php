<?php

namespace App\Services;

use App\Model\Transaction\TransactionLog;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TransactionLogService
{
    public function createCreditNoteForRide($data){
        $TransactionLog=TransactionLog::create($data);
        return $TransactionLog;
    }

    public function getCreditNoteForRide($data){
        try{
            $TransactionLog = TransactionLog::where("request_id",$data->request_id)->firstOrFail();

            return ['message'=>"OK","data"=>["transaction_log_id"=>$TransactionLog->transaction_log_id,"status"=>$TransactionLog->status,"TransactionLog"=>$TransactionLog],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }

    }
    public function getCreditNoteForRideForTripHistory($data){
        try{
            $TransactionLog = TransactionLog::select("cost","distance_miles","currency","promo_code_value","promo_code")->where("request_id",$data->request_id)->firstOrFail();

            return ['message'=>"OK","data"=>["transaction_log_id"=>$TransactionLog->transaction_log_id,"status"=>$TransactionLog->status,"TransactionLog"=>$TransactionLog],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }

    }
    public function updateCreditNoteStatusForRide($data){
        try{
          //  print_r($data); exit;
            $TransactionLog = TransactionLog::where("request_id",$data->request_id)->firstOrFail();
            $TransactionLog->status="COMPLETED";
            if($data->payment_method=="CARD"){
                $data->payment_gateway_transaction_id=$data->trans_id;
            }

            $TransactionLog->save();

            return ['message'=>"OK","data"=>["transaction_log_id"=>$TransactionLog->transaction_log_id],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }

    }
}



