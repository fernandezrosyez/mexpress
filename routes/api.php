<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//Route::group(['middleware' => ['cors']], function () {
    Route::get('get/service/list', 'Api\ServiceTypeMstController@get')->name("get.servicemst");
    Route::get('get/model/list', 'Api\VehicleMst\VehicleMstController@get')->name("get.servicemst");
    Route::get('get/make/service', 'Api\VehicleMst\VehicleMstController@getMake')->name("get.make.service");
    Route::post('get/model/service', 'Api\VehicleMst\VehicleMstController@getModel')->name("get.model.service");
    Route::post('get/year/service', 'Api\VehicleMst\VehicleMstController@getYear')->name("get.year.service");

    //isEmailverified
    Route::get('passenger/emailverify/{id}', 'Api\Passenger\ProfileController@verifyEmailID')->name("passenger.emailverify");
    
    Route::get('driver/emailverify/{id}', 'Api\Driver\ProfileController@verifyEmailID')->name("driver.emailverify");

    //web
    Route::get('setting/bannerText', 'Api\WebService\WebController@getBannerText')->name("setting.bannerText");
    Route::get('setting/aboutUs', 'Api\WebService\WebController@getAboutUS')->name("setting.aboutUs");
    Route::get('setting/appParagraph', 'Api\WebService\WebController@getAppParagraph')->name("setting.appParagraph");
    Route::get('setting/termCondition', 'Api\WebService\WebController@getTermsConditions')->name("setting.termCondition");
    Route::get('setting/privacyPolicy', 'Api\WebService\WebController@getPrivacyPolicy')->name("setting.privacyPolicy");
    
   	Route::post('contact/us', 'Api\WebService\WebController@contactUS')->name("contact.us");
//});
