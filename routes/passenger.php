<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::group(['middleware' => ['cors']], function () {
    Route::post('passenger/signup/otp', 'Api\Passenger\RegistrationController@onlyOTP')->name("passenger.registration.onlyOTP");
    Route::post('passenger/verify/otp', 'Api\Passenger\RegistrationController@verifyOTP')->name("passenger.verify.otp");
    Route::post('passenger/verify/otp/password', 'Api\Passenger\AuthController@verifyOTPForgetPassword')->name("passenger.verify.otp.password");

    Route::post('passenger/signup', 'Api\Passenger\RegistrationController@register')->name("passenger.registration");
    Route::post('passenger/login', 'Api\Passenger\AuthController@login')->name("passenger.login");
    Route::post('passenger/forget/password', 'Api\Passenger\AuthController@forgetPassword')->name("passenger.forgetPassword");
    Route::post('passenger/reset/password', 'Api\Passenger\AuthController@resetPassword')->name("passenger.resetPassword");
    Route::post('payment/verify/transaction', 'Api\Payment\PayStackController@verifyTransaction')->name("verifyTransaction");

    Route::group(['middleware' => ['auth:api','scope:temporary-customer-service']], function () {
        //  //Emergency Contact
        //  Route::post('passenger/emergencyContact', 'Api\Passenger\ProfileController@emergencyContact')->name("passenger.emergencyContact");
        // //Mobile no update and verification
        Route::post('passenger/mobile', 'Api\Passenger\ProfileController@updateMobileNo')->name("passenger.mobile");
        Route::put('passenger/mobile/verify', 'Api\Passenger\ProfileController@verifyMobileNo')->name("passenger.mobileVerify");
        Route::get('passenger/mobile/reverify', 'Api\Passenger\ProfileController@resetMobileVerificationOtp')->name("passenger.mobileReVerify");
    });

    Route::group(['middleware' => ['auth:api','scope:passenger-service']], function () {
        //     //logout
        Route::get('passenger/logout', 'Api\Passenger\AuthController@logout')->name("passenger.logout");

        //     //profile images update
        Route::post('passenger/profile/image', 'Api\Passenger\ProfileController@profileImageUpdate')->name("passenger.profileImage");

        //     //profile update  getProfile
        Route::get('passenger/profile', 'Api\Passenger\ProfileController@getProfile')->name("passenger.profile");
        Route::post('passenger/profile/update', 'Api\Passenger\ProfileController@updateProfile')->name("passenger.updateProfile");

        //     // password
        Route::post('passenger/change/password', 'Api\Passenger\ProfileController@changePassword')->name("passenger.changePassword");


        /**** Stripe  */
        Route::post('passenger/add/card', 'Api\Passenger\PaymentController@addCard')->name("passenger.addCard");
        Route::get('passenger/card', 'Api\Passenger\PaymentController@ListCard')->name("passenger.ListCard");
        Route::delete('passenger/card', 'Api\Passenger\PaymentController@DeleteCard')->name("passenger.DeleteCard");
        Route::put('passenger/default/card', 'Api\Passenger\PaymentController@setDefaultCard')->name("passenger.setDefaultCard");

        // background api
        Route::post('passenger/background/data', 'Api\Background\PassengerBackgroundController@get')->name("passenger.backgroundDb");


        ##booking##
        Route::post('passenger/get/estimated/fare','Api\Passenger\RequestController@estimated')->name("passenger.estimatedFare");
        Route::post('passenger/request/ride','Api\Passenger\RequestController@requestRide')->name("passenger.requestRide");
        Route::post('passenger/request/ratingComment','Api\Passenger\RequestController@ratingComment')->name("passenger.ratingComment");

        Route::post('passenger/request/schedule/ride','Api\Passenger\RequestController@scheduleRide')->name("passenger.scheduleRide");
        Route::post('passenger/request/schedule/cancel','Api\Passenger\RequestController@scheduleCancel')->name("passenger.scheduleCancel");



        // payment card

        Route::post('passenger/card/payment', 'Api\Passenger\PaymentController@makeCardPayment')->name("passenger.makeCardPayment");

        //message
        Route::post('passenger/on/ride/message', 'Api\Driver\MessageController@sendMessage')->name("passenger.trip.sendMessage");
        Route::post('passenger/on/ride/get/message', 'Api\Driver\MessageController@getMessage')->name("passenger.trip.getMessage");

        // Trip History
        Route::get('passenger/trip/history', 'Api\Passenger\RequestController@tripHistory')->name("passenger.trip.tripHistory");
        Route::get('passenger/upcoming/schedule', 'Api\Passenger\RequestController@upCommingScheduleTrip')->name("passenger.trip.upCommingScheduleTrip");
        Route::post('passenger/trip/details', 'Api\Passenger\RequestController@tripDetails')->name("passenger.trip.tripDetails");

        // report issue
        Route::post('passenger/report/issue','Api\ReportIssue\ReportIssueController@reportIssue')->name('passenger.reportIssue');
        Route::post('passenger/report/issue/subject','Api\ReportIssue\ReportIssueController@getSubject')->name('passenger.getSubject');

        // promocode
        Route::post('passenger/add/promo', 'Api\Promocode\PromocodeController@addPromoCode')->name("passenger.addPromoCode");
        Route::get('passenger/get/promo', 'Api\Promocode\PromocodeController@getPromoCode')->name("passenger.getPromoCode");

        // notification
        Route::get('passenger/get/notification','Api\Driver\NotificationController@getNotification')->name("passenger.getNotification");


    });
//});
