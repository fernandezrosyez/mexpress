<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

//Route::group(['middleware' => ['cors']], function () {
    Route::post('driver/signup', 'Api\Driver\RegistrationController@register')->name("driver.registration");
    Route::post('driver/signup/otp', 'Api\Driver\RegistrationController@onlyOTP')->name("driver.registration.onlyOTP");
    Route::post('driver/verify/otp', 'Api\Driver\RegistrationController@verifyOTP')->name("driver.verify.otp");
    Route::post('driver/verify/otp/password', 'Api\Driver\AuthController@verifyOTPForgetPassword')->name("driver.verify.otp.password");

    Route::post('driver/login', 'Api\Driver\AuthController@login')->name("Driver.login");
    Route::post('driver/forget/password', 'Api\Driver\AuthController@forgetPassword')->name("Driver.forgetPassword");
    Route::post('driver/reset/password', 'Api\Driver\AuthController@resetPassword')->name("Driver.resetPassword");
    Route::group(['middleware' => ['auth:api','scope:temporary-customer-service']], function () {
        //Mobile no update and verification
        Route::post('driver/mobile', 'Api\Driver\ProfileController@updateMobileNo')->name("driver.mobile");
        Route::put('driver/mobile/verify', 'Api\Driver\ProfileController@verifyMobileNo')->name("driver.mobileVerify");
        Route::get('driver/mobile/reverify', 'Api\Driver\ProfileController@resetMobileVerificationOtp')->name("driver.mobileReVerify");
    });
    Route::group(['middleware' => ['auth:api','scope:driver-service']], function () {
        //logout
        Route::get('driver/logout', 'Api\Driver\AuthController@logout')->name("driver.logout");

        //email update
        Route::post('driver/email', 'Api\Driver\ProfileController@updatEmail')->name("driver.email");

        //profile images update
        Route::post('driver/profile/image', 'Api\Driver\ProfileController@profileImageUpdate')->name("driver.profileImage");

        //profile update  getProfile
        Route::get('driver/profile', 'Api\Driver\ProfileController@getProfile')->name("driver.profile");
        Route::post('driver/profile/update', 'Api\Driver\ProfileController@updateProfile')->name("driver.updateProfile");
        Route::put('driver/on/off/update', 'Api\Driver\ProfileController@onOff')->name("driver.onOff");

        // document update
        Route::get('driver/document/list', 'Api\Driver\DocumentController@list')->name("driver.document.list");
        Route::post('driver/document/upload/{document_id}', 'Api\Driver\DocumentController@uploadDocument')->name("driver.document.upload");
        // Route::get('driver/document/completion/percentage', 'Api\Driver\StatusController@DocumentCompletion')->name("DocumentCompletion");

        // password
        Route::post('driver/change/password', 'Api\Driver\ProfileController@changePassword')->name("driver.changePassword");

        // background api
        Route::post('driver/background/data', 'Api\Background\DriversBackgroundController@get')->name("driver.backgroundDb");

        Route::post('driver/request/reject', 'Api\Driver\TripController@reject')->name("driver.request.reject");
        Route::post('driver/trip/control', 'Api\Driver\TripController@tripControl')->name("driver.trip.control");
        Route::post('driver/get/signature', 'Api\Driver\TripController@signature')->name("driver.trip.signature");
        Route::post('driver/upload/parcel/image', 'Api\Driver\TripController@createParcelImage')->name("driver.trip.createParcelImage");
        Route::post('driver/get/parcel/image', 'Api\Driver\TripController@getParcleImage')->name("driver.trip.getParcelImage");

        //message
        Route::post('driver/on/ride/message', 'Api\Driver\MessageController@sendMessage')->name("driver.trip.sendMessage");
        Route::post('driver/on/ride/get/message', 'Api\Driver\MessageController@getMessage')->name("driver.trip.getMessage");

        // Trip History
        Route::get('driver/trip/history', 'Api\Driver\TripController@tripHistory')->name("driver.trip.tripHistory");
        Route::post('driver/ride/details', 'Api\Driver\TripController@tripDetails')->name("driver.trip.tripDetails");

          // report issue
        Route::post('driver/report/issue','Api\ReportIssue\ReportIssueController@reportIssue')->name('driver.reportIssue');
        Route::post('driver/report/issue/subject','Api\ReportIssue\ReportIssueController@getSubject')->name('driver.getSubject');


        // wallet
        Route::get('driver/get/payment/statistics','Api\Payment\PaymentController@getDriverPaymentStatactics')->name('driver.getDriverPaymentStatactics');
        Route::get('driver/get/payment/list','Api\Payment\PaymentController@getDriverPaymentList')->name('driver.getDriverPaymentList');

        // notification
        Route::get('driver/get/notification','Api\Driver\NotificationController@getNotification')->name("driver.getNotification");



    });
//});

